var Game = require('../models/game');
var User = require('../models/user');
var sessionMiddleware = require('../passport/session');

var gamesList = {};

var newUserJoined = function(game, user, callback) {
	if (game._id in gamesList) {
		gamesList[game._id][user._id].active = true;
		callback(gamesList[game._id]);
	} else {
		var userIds = [];
		for (var player of game.players) {
			userIds.push(player.user);
		}
		User.find({ '_id': { $in: userIds } }, function(err, users) {
			gamesList[game._id] = {};
			for (var it of users) {
				gamesList[game._id][it._id] = {
					'username': it.username,
					'active': false
				};
			}
			gamesList[game._id][user._id].active = true;
			callback(gamesList[game._id]);
		});
	}
}

var userDisconnected = function(gameId, userId, callback) {
	if (gameId in gamesList) {
		gamesList[gameId][userId].active = false;
		callback(gamesList[gameId]);
	}
}

module.exports = function(app) {
	var io = require('socket.io')(app);

	io.use(function(socket, next){
		sessionMiddleware(socket.request, {}, next);
    });

	io.on('connection', function(socket) {
		var user;
		var gameId;

		var findGame = function(callback) {
			Game.findOne({ '_id': gameId }, function(err, game) {
				if (!err && game) {
					callback(game);
				}
			});
		}

		var saveGame = function(game) {
			game.save();
			io.to(gameId).emit('update', game);
		}

		var getPlayer = function(game, userId) {
			for (var player of game.players) {
				if (player.user.equals(user._id)) {
					return player;
				}
			}
		}

		if (socket.request.session.passport) {
			var userId = socket.request.session.passport.user;
			gameId = socket.request.session.gameId;
        	console.log('Socket: Your User ID is', userId);
        	console.log('Socket: Your game ID is', gameId);

        	User.findOne({ '_id':  userId }, function(err, u) {
				if (!err && u) {
					user = u;
					Game.findOne({'_id': gameId }, function(err, game) {
						if (!err && game) {
							newUserJoined(game, user, function(users) {
								console.log('Socket: User connected');
								socket.join(gameId);
								io.to(gameId).emit('users', users);
								socket.emit('init', { userId: userId });
								socket.emit('update', game);
							})
						} else {
							console.log('Socket: User connected but game was not found');
							socket.disconnect();
						}
					});
				} else {
					console.log('Socket: Not authorized user connected');
					socket.disconnect();
				}
			});
		} else {
			console.log('Socket: Not authenticated connection');
			socket.disconnect();
		}

		socket.on('disconnect', function() {
			console.log('Socket: User disconnected');
			userDisconnected(gameId, userId, function(users) {
				io.to(gameId).emit('users', users);
			});
		});

		socket.on('region', function(region) {
			findGame(function(game) {
				game.chooseRegion(user._id, region);
				saveGame(game);
			});
		});

		socket.on('pick-plant', function(plantChoice) {
			findGame(function(game) {
				var plant = game.stack[plantChoice];
				game.pickPlantForAuction(user, plant);
				saveGame(game);
			});
		});

		socket.on('bid-plant', function(amount) {
			findGame(function(game) {
				if (amount != -1) {
					game.bidForPlant(user, amount);
				} else {
					game.passForPlant(user);
				}
				saveGame(game);
			});
		});

		socket.on('drop-plant', function(plant) {
			findGame(function(game) {
				var player = getPlayer(game, user);
				console.log('Plant:', plant);
				console.log('Plants:', player.plants);
				console.log('Selected:',player.plants[plant]);
				game.discardPlant(user, player.plants[plant]);
				saveGame(game);
			});
		})

		socket.on('buy-resources', function(order) {
			findGame(function(game) {
				var player = getPlayer(game, user);
				game.buyResources(order.resource, user, player.plants[order.plant], order.amount);
				saveGame(game);
			});
		});

		socket.on('move-resources', function(order) {
			findGame(function(game) {
				var player = getPlayer(game, user);
				game.moveResourceBetweenPlants(order.resource, order.amount, player.plants[order.from], player.plants[order.to]);
				saveGame(game);
			});
		});

		socket.on('end-resources', function() {
			findGame(function(game) {
				game.endResourceBuy(user);
				saveGame(game);
			});
		});

		socket.on('buy-stations', function(order) {
			findGame(function(game) {
				game.buyCitiesForUser(user, order);
				saveGame(game);
			});
		});

		socket.on('power-plan', function(plan) {
			findGame(function(game) {
				game.powerCities(user, plan);
				if (game.state == 'end') {
					var winner = game.whoWon();
					io.to(gameId).emit('winner', winner);
					io.to(gameId).emit('update', game);
					game.remove();
				} else {
					saveGame(game);
				}
			});
		});
	});
}