var express = require('express');
var router = express.Router();

var Lobby = require('../models/lobby');
var Game = require('../models/game');
var Map = require('../models/map');

var isAuthenticated = function (req, res, next) {
	if (req.isAuthenticated())
		return next();
	res.redirect('/login');
}

var findOnList = function(model, list) {
	var found = false;
	for (var it of list) {
		if (it._id.equals(model._id)) {
			return true;
		}
	}
	return false;
}

module.exports = function(passport){

	router.get('/login', function(req, res) {
		res.render('index', { message: req.flash('message') });
	});

	router.post('/login', passport.authenticate('login', {
		successRedirect: '/',
		failureRedirect: '/login',
		failureFlash : true  
	}));

	router.get('/signup', function(req, res){
		res.render('register',{message: req.flash('message')});
	});

	router.post('/signup', passport.authenticate('signup', {
		successRedirect: '/',
		failureRedirect: '/signup',
		failureFlash : true  
	}));

	router.get('/signout', function(req, res) {
		req.logout();
		res.redirect('/login');
	});

	router.get('/', isAuthenticated, function(req, res) {
		Lobby.find({})
		.populate('players owner')
		.exec(function(err, lobbies) {
			if (err) {
				console.log('Lobbies Not Found!');
				res.redirect('/');
				return
			}
			var ownedLobbies = [];
			var joinedLobbies = [];
			var otherLobbies = [];
			for (var lobby of lobbies) {
				if (lobby.owner._id.equals(req.user._id)) {
					ownedLobbies.push(lobby);
				} else {
					if (findOnList(req.user, lobby.players)) {
						joinedLobbies.push(lobby);
					} else {
						otherLobbies.push(lobby);
					}
				}
			}
			Game.find({'players.user': {$all:[req.user]}})
			.populate('players.user')
			.exec(function(err, games) {
				if (err) {
					console.log('Games Not Found!');
					res.redirect('/');
					return
				}
				res.render('home', { user: req.user, games: games, ownedLobbies: ownedLobbies, joinedLobbies: joinedLobbies, otherLobbies: otherLobbies });
			});
		});
	});

	router.post('/new_game', isAuthenticated, function(req, res) {
		var newGame = new Lobby();
		newGame.name = req.body.name;
		newGame.maxPlayers = req.body.max;
		newGame.owner = req.user._id;
		newGame.players.push(req.user._id);
		if (newGame.maxPlayers >= 2 && newGame.maxPlayers <= 6) {
			Map.findOne({ name: 'USA' })
        	.exec(function(err, map) {
				if (err) {
					console.log('Error in finding map in NewGame: '+err);
					return;
				}
				newGame.setMap(map);
				newGame.save(function(err) {
					if (err) {
						console.log('Error in NewGame: '+err);
					}
				});
			});
		} else {
			console.log('Error in NewGame: wrong player number '+newGame.maxPlayers);
		}
		res.redirect('/');
	});

	router.get('/join/:lobbyId', isAuthenticated, function(req, res) {
		Lobby.findOne({ '_id': req.params.lobbyId })
		.populate('players')
		.exec(function(err, lobby) {
			if (err) {
				console.log('Lobby to join Not Found!');
				res.redirect('/');
				return
			}
			if (lobby.maxPlayers > lobby.players.length && !findOnList(req.user, lobby.players)) {
				lobby.players.push(req.user._id);
				lobby.save(function(err) {
					if (err) {
						console.log('Error when joining lobby: '+lobby);
					}
				});
			}
			res.redirect('/');
		});
	});

	router.get('/leave/:lobbyId', isAuthenticated, function(req, res) {
		Lobby.findOne({ '_id': req.params.lobbyId })
		.populate('players')
		.exec(function(err, lobby) {
			if (err) {
				console.log('Lobbiy to leave Not Found!');
				res.redirect('/');
				return
			}
			var index;
			for (index = 0; index < lobby.players.length; ++index) {
				if (lobby.players[index].equals(req.user._id)) {
					break;
				}
			}
			if (index < lobby.players.length) {
				lobby.players.splice(index, 1);
				lobby.save(function(err) {
					if (err) {
						console.log('Error when leaving lobby: '+lobby);
					}
				});
			}
			res.redirect('/');
		});
	});

	router.get('/remove/:lobbyId', isAuthenticated, function(req, res) {
		Lobby.findOne({ '_id': req.params.lobbyId })
		.populate('owner')
		.exec(function(err, lobby) {
			if (err) {
				console.log('Lobbiy to remove Not Found!');
				res.redirect('/');
				return
			}
			if (lobby.owner._id.equals(req.user._id)) {
				lobby.remove(function(err) {
					if (err) {
						console.log('Error when removing lobby: '+lobby);
					}
				});
			}
			res.redirect('/');
		});
	});

	router.get('/start/:lobbyId', isAuthenticated, function(req, res) {
		Lobby.findOne({ '_id': req.params.lobbyId })
		.populate('owner')
		.exec(function(err, lobby) {
			if (err) {
				console.log('Lobbiy to start Not Found!');
				res.redirect('/');
				return
			}
			if (lobby.players.length > 1 && lobby.owner._id.equals(req.user._id)) {
				var newGame = new Game();
				newGame.initGame(lobby);
				newGame.save(function(err) {
					if (err) {
						console.log('Error when starting game: '+newGame);
					}
					lobby.remove(function(err) {
						if (err) {
							console.log('Error when removing started lobby: '+lobby);
						}
					});
				});
			}
			res.redirect('/');
		});
	});

	router.get('/game/:gameId', isAuthenticated, function(req, res) {
		req.session.gameId = req.params.gameId;
		res.render('game', {});
	});

	return router;
}