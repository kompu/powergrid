conn = new Mongo();
db = conn.getDB("test");

var plants = [];

plants.push({ 
    cost: 4,
    requirement: 2,
    power: 1,
    plantType: 'coal',
    image: '/plants/4.jpeg'
});
plants.push({ 
    cost: 8,
    requirement: 3,
    power: 2,
    plantType: 'coal',
    image: '/plants/8.jpeg'
});
plants.push({ 
    cost: 10,
    requirement: 2,
    power: 2,
    plantType: 'coal',
    image: '/plants/10.jpeg'
});
plants.push({ 
    cost: 15,
    requirement: 2,
    power: 3,
    plantType: 'coal',
    image: '/plants/15.jpeg'
});
plants.push({ 
    cost: 20,
    requirement: 3,
    power: 5,
    plantType: 'coal',
    image: '/plants/20.jpeg'
});
plants.push({ 
    cost: 25,
    requirement: 2,
    power: 5,
    plantType: 'coal',
    image: '/plants/25.jpeg'
});
plants.push({ 
    cost: 31,
    requirement: 3,
    power: 6,
    plantType: 'coal',
    image: '/plants/31.jpeg'
});
plants.push({ 
    cost: 36,
    requirement: 3,
    power: 7,
    plantType: 'coal',
    image: '/plants/36.jpeg'
});
plants.push({ 
    cost: 42,
    requirement: 2,
    power: 6,
    plantType: 'coal',
    image: '/plants/42.jpeg'
});
plants.push({ 
    cost: 3,
    requirement: 2,
    power: 1,
    plantType: 'oil',
    image: '/plants/3.jpeg'
});
plants.push({ 
    cost: 7,
    requirement: 3,
    power: 2,
    plantType: 'oil',
    image: '/plants/7.jpeg'
});
plants.push({ 
    cost: 9,
    requirement: 1,
    power: 1,
    plantType: 'oil',
    image: '/plants/9.jpeg'
});
plants.push({ 
    cost: 16,
    requirement: 2,
    power: 3,
    plantType: 'oil',
    image: '/plants/16.jpeg'
});
plants.push({ 
    cost: 26,
    requirement: 2,
    power: 5,
    plantType: 'oil',
    image: '/plants/26.jpeg'
});
plants.push({ 
    cost: 32,
    requirement: 3,
    power: 6,
    plantType: 'oil',
    image: '/plants/32.jpeg'
});
plants.push({ 
    cost: 35,
    requirement: 1,
    power: 5,
    plantType: 'oil',
    image: '/plants/35.jpeg'
});
plants.push({ 
    cost: 40,
    requirement: 2,
    power: 6,
    plantType: 'oil',
    image: '/plants/40.jpeg'
});
plants.push({ 
    cost: 5,
    requirement: 2,
    power: 1,
    plantType: 'hybrid',
    image: '/plants/5.jpeg'
});
plants.push({ 
    cost: 12,
    requirement: 2,
    power: 2,
    plantType: 'hybrid',
    image: '/plants/12.jpeg'
});
plants.push({ 
    cost: 21,
    requirement: 2,
    power: 4,
    plantType: 'hybrid',
    image: '/plants/21.jpeg'
});
plants.push({ 
    cost: 29,
    requirement: 1,
    power: 4,
    plantType: 'hybrid',
    image: '/plants/29.jpeg'
});
plants.push({ 
    cost: 46,
    requirement: 3,
    power: 7,
    plantType: 'hybrid',
    image: '/plants/46.jpeg'
});
plants.push({ 
    cost: 6,
    requirement: 1,
    power: 1,
    plantType: 'garbage',
    image: '/plants/6.jpeg'
});
plants.push({ 
    cost: 14,
    requirement: 2,
    power: 2,
    plantType: 'garbage',
    image: '/plants/14.jpeg'
});
plants.push({ 
    cost: 19,
    requirement: 2,
    power: 3,
    plantType: 'garbage',
    image: '/plants/19.jpeg'
});
plants.push({ 
    cost: 24,
    requirement: 2,
    power: 4,
    plantType: 'garbage',
    image: '/plants/24.jpeg'
});
plants.push({ 
    cost: 30,
    requirement: 3,
    power: 6,
    plantType: 'garbage',
    image: '/plants/30.jpeg'
});
plants.push({ 
    cost: 38,
    requirement: 3,
    power: 7,
    plantType: 'garbage',
    image: '/plants/38.jpeg'
});
plants.push({ 
    cost: 11,
    requirement: 1,
    power: 2,
    plantType: 'nuclear',
    image: '/plants/11.jpeg'
});
plants.push({ 
    cost: 17,
    requirement: 1,
    power: 2,
    plantType: 'nuclear',
    image: '/plants/17.jpeg'
});
plants.push({ 
    cost: 23,
    requirement: 1,
    power: 3,
    plantType: 'nuclear',
    image: '/plants/23.jpeg'
});
plants.push({ 
    cost: 28,
    requirement: 1,
    power: 4,
    plantType: 'nuclear',
    image: '/plants/28.jpeg'
});
plants.push({ 
    cost: 34,
    requirement: 1,
    power: 5,
    plantType: 'nuclear',
    image: '/plants/34.jpeg'
});
plants.push({ 
    cost: 39,
    requirement: 1,
    power: 6,
    plantType: 'nuclear',
    image: '/plants/39.jpeg'
});
plants.push({ 
    cost: 13,
    requirement: 0,
    power: 1,
    plantType: 'green',
    image: '/plants/13.jpeg'
});
plants.push({ 
    cost: 18,
    requirement: 0,
    power: 2,
    plantType: 'green',
    image: '/plants/18.jpeg'
});
plants.push({ 
    cost: 22,
    requirement: 0,
    power: 2,
    plantType: 'green',
    image: '/plants/22.jpeg'
});
plants.push({ 
    cost: 27,
    requirement: 0,
    power: 3,
    plantType: 'green',
    image: '/plants/27.jpeg'
});
plants.push({ 
    cost: 33,
    requirement: 0,
    power: 4,
    plantType: 'green',
    image: '/plants/33.jpeg'
});
plants.push({ 
    cost: 37,
    requirement: 0,
    power: 4,
    plantType: 'green',
    image: '/plants/37.jpeg'
});
plants.push({ 
    cost: 44,
    requirement: 0,
    power: 5,
    plantType: 'green',
    image: '/plants/44.jpeg'
});
plants.push({ 
    cost: 50,
    requirement: 0,
    power: 6,
    plantType: 'green',
    image: '/plants/50.jpeg'
});

var Map = db.maps;
Map.drop();

var graph = {};

function newCity(name, zone) {
    var city = {
        'id': -1,
        'name': name,
        'zone': zone,
        'connections': []
    };
    graph[name] = city;
}

function giveIds() {
    var id = 0;
    for (key in graph) {
        graph[key].id = id;
        ++id;
    }
}

function newConnection(name1, name2, cost) {
    if (graph[name1] === undefined || graph[name2] === undefined) {
        print('Undefined connection! ' + name1 + ', ' + name2);
    }
    var first = graph[name1];
    var second = graph[name2];
    first.connections.push({ 'target': second.id, 'cost': cost });
    second.connections.push({ 'target': first.id, 'cost': cost });
}

function printSizes() {
    for (var key in graph) {
        print(key + ': ' + graph[key].connections.length);
    }
}

function createListFromGraph() {
    var list = [];
    for (var key in graph) {
        list.push(graph[key]);
    }
    return list;
}

newCity('Seatle', 1);
newCity('Portland', 1);
newCity('Boise', 1);
newCity('Billings', 1);
newCity('Cheyenne', 1);
newCity('Denver', 1);
newCity('Omaha', 1);

newCity('San Francisco', 2);
newCity('Los Angeles', 2);
newCity('San Diego', 2);
newCity('Las Vegas', 2);
newCity('Salt Lake City', 2);
newCity('Phoenix', 2);
newCity('Santa Fe', 2);

newCity('Kansas City', 3);
newCity('Oklahoma City', 3);
newCity('Dallas', 3);
newCity('Houston', 3);
newCity('Memphis', 3);
newCity('New Orleans', 3);
newCity('Birmingham', 3);

newCity('Fargo', 4);
newCity('Duluth', 4);
newCity('Mineapolis', 4);
newCity('Chicago', 4);
newCity('St. Louis', 4);
newCity('Cincinnati', 4);
newCity('Knoxville', 4);

newCity('Detroit', 5);
newCity('Buffalo', 5);
newCity('Pittsburgh', 5);
newCity('Washington', 5);
newCity('Philadelphia', 5);
newCity('New York', 5);
newCity('Boston', 5);

newCity('Norfolk', 6);
newCity('Raleigh', 6);
newCity('Atlanta', 6);
newCity('Savannah', 6);
newCity('Jacksonville', 6);
newCity('Tampa', 6);
newCity('Miami', 6);

giveIds();
newConnection('Seatle', 'Portland', 3);
newConnection('Seatle', 'Boise', 12);
newConnection('Seatle', 'Billings', 9);
newConnection('Portland', 'Boise', 13);
newConnection('Portland', 'San Francisco', 24);
newConnection('San Francisco', 'Boise', 23);
newConnection('San Francisco', 'Salt Lake City', 27);
newConnection('San Francisco', 'Las Vegas', 14);
newConnection('San Francisco', 'Los Angeles', 9);
newConnection('Los Angeles', 'Las Vegas', 9);
newConnection('Los Angeles', 'San Diego', 3);
newConnection('Boise', 'Billings', 12);
newConnection('Boise', 'Cheyenne', 24);
newConnection('Boise', 'Salt Lake City', 8);
newConnection('Salt Lake City', 'Denver', 21);
newConnection('Salt Lake City', 'Santa Fe', 28);
newConnection('Las Vegas', 'Salt Lake City', 18);
newConnection('Las Vegas', 'Phoenix', 15);
newConnection('Las Vegas', 'Santa Fe', 27);
newConnection('Las Vegas', 'San Diego', 9);
newConnection('San Diego', 'Phoenix', 14);
newConnection('Phoenix', 'Santa Fe', 18);
newConnection('Billings', 'Cheyenne', 9);
newConnection('Billings', 'Fargo', 17);
newConnection('Billings', 'Mineapolis', 18);
newConnection('Cheyenne', 'Omaha', 14);
newConnection('Cheyenne', 'Denver', 0);
newConnection('Cheyenne', 'Mineapolis', 18);
newConnection('Denver', 'Kansas City', 16);
newConnection('Denver', 'Santa Fe', 13);
newConnection('Santa Fe', 'Kansas City', 16);
newConnection('Santa Fe', 'Oklahoma City', 15);
newConnection('Santa Fe', 'Dallas', 16);
newConnection('Santa Fe', 'Houston', 21);
newConnection('Fargo', 'Duluth', 6);
newConnection('Fargo', 'Mineapolis', 6);
newConnection('Duluth', 'Mineapolis', 5);
newConnection('Duluth', 'Chicago', 12);
newConnection('Duluth', 'Detroit', 15);
newConnection('Mineapolis', 'Omaha', 8);
newConnection('Mineapolis', 'Chicago', 8);
newConnection('Omaha', 'Chicago', 13);
newConnection('Omaha', 'Kansas City', 5);
newConnection('Kansas City', 'Chicago', 8);
newConnection('Kansas City', 'St. Louis', 6);
newConnection('Kansas City', 'Memphis', 12);
newConnection('Kansas City', 'Oklahoma City', 8);
newConnection('Oklahoma City', 'Memphis', 14);
newConnection('Oklahoma City', 'Dallas', 3);
newConnection('Dallas', 'Memphis', 12);
newConnection('Dallas', 'New Orleans', 12);
newConnection('Dallas', 'Houston', 5);
newConnection('Houston', 'New Orleans', 8);
newConnection('Chicago', 'Detroit', 7);
newConnection('Chicago', 'Cincinnati', 7);
newConnection('Chicago', 'St. Louis', 10);
newConnection('St. Louis', 'Cincinnati', 12);
newConnection('St. Louis', 'Atlanta', 12);
newConnection('St. Louis', 'Memphis', 7);
newConnection('Memphis', 'Birmingham', 6);
newConnection('Memphis', 'New Orleans', 7);
newConnection('New Orleans', 'Birmingham', 11);
newConnection('New Orleans', 'Jacksonville', 16);
newConnection('Birmingham', 'Atlanta', 7);
newConnection('Birmingham', 'Jacksonville', 9);
newConnection('Detroit', 'Buffalo', 7);
newConnection('Detroit', 'Cincinnati', 4);
newConnection('Detroit', 'Pittsburgh', 6);
newConnection('Cincinnati', 'Pittsburgh', 7);
newConnection('Cincinnati', 'Raleigh', 15);
newConnection('Cincinnati', 'Knoxville', 6);
newConnection('Knoxville', 'Atlanta', 5);
newConnection('Atlanta', 'Raleigh', 7);
newConnection('Atlanta', 'Savannah', 7);
newConnection('Buffalo', 'New York', 8);
newConnection('Buffalo', 'Pittsburgh', 7);
newConnection('Pittsburgh', 'Washington', 6);
newConnection('Pittsburgh', 'Raleigh', 7);
newConnection('Boston', 'New York', 3);
newConnection('New York', 'Philadelphia', 0);
newConnection('Philadelphia', 'Washington', 3);
newConnection('Washington', 'Norfolk', 5);
newConnection('Norfolk', 'Raleigh', 3);
newConnection('Raleigh', 'Savannah', 7);
newConnection('Savannah', 'Jacksonville', 0);
newConnection('Jacksonville', 'Tampa', 4);
newConnection('Tampa', 'Miami', 4);

var cityList = createListFromGraph();

var regions = [{
    id: 1,
    name: 'Purple',
    connections: [2, 3, 4]
}, {
    id: 2,
    name: 'Cyan',
    connections: [1, 3]
}, {
    id: 3,
    name: 'Red',
    connections: [1, 2, 4, 6]
}, {
    id: 4,
    name: 'Yellow',
    connections: [1, 3, 5, 6]
}, {
    id: 5,
    name: 'Brown',
    connections: [4, 6]
}, {
    id: 6,
    name: 'Green',
    connections: [3, 4, 5]
}]

Map.insert({
    name: 'USA',
    image: '/maps/usa.jpg',
    css: 'map-usa',
    cities: cityList,
    plants: plants,
    regions: regions
});