var mongoose = require('mongoose');
var mapSchema = require('./map_schema');

var lobbySchema = mongoose.Schema({
    name: String,
    maxPlayers: Number,
    map: mapSchema,
    owner: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
    players: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }]
});

lobbySchema.methods.setMap = function(map) {
    this.map = {
        name: map.name,
        image: map.image,
        css: map.css,
        cities: map.cities,
        plants: map.plants,
        regions: map.regions
    }
}

module.exports = lobbySchema;