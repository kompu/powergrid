var mongoose = require('mongoose');

module.exports = mongoose.Schema({
    username: String,
    password: String,
    email: String
});