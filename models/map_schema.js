var mongoose = require('mongoose');
var plantSchema = require('./plant_schema');
var citySchema = require('./city_schema');

module.exports = mongoose.Schema({
    name: String,
    image: String,
    css: String,
    cities: [citySchema],
    plants: [plantSchema],
    regions: [{
        id: Number,
        name: String,
        connections: [Number]
    }]
});