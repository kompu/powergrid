module.exports = {
	MAX_RESOURCES: 24,
	MAX_URANIUM: 12,

	COAL: 'coal',
	OIL: 'oil',
	GARBAGE: 'garbage',
	NUCLEAR: 'nuclear',
	GREEN: 'green',
	HYBRID: 'hybrid',
	plantTypes: [this.COAL, this.OIL, this.GARBAGE, this.NUCLEAR, this.GREEN, this.HYBRID],
	URANIUM: 'uranium',

	REGIONS: 'regions',
	AUCTION: 'auction',
	RESOURCES: 'resources',
	STATIONS: 'stations',
	BUREAUCRACY: 'bureaucracy',
	END: 'end',
	gameStates: [this.REGIONS, this.AUCTION, this.RESOURCES, this.STATIONS, this.BUREAUCRACY, this.END],

	powerPays: [10, 22, 33, 44, 54, 64, 73, 82, 90, 98, 105, 112, 118, 124, 129, 134, 138, 142, 145, 148, 150, 152, 154, 156, 158, 160, 162, 164, 166, 168, 170],

	removePlants: [8, 8, 4, 0, 0],
	availablePlants: [4, 3, 3, 3, 3],
	requiredCities: [10, 7, 7, 7, 6],

	coalRenewal: [[3, 4, 3], [4, 5, 3], [5, 6, 4], [5, 7, 5], [7, 9, 6]],
	oilRenewal: [[2, 2, 4], [2, 3, 4], [3, 4, 5], [4, 5, 6], [5, 6, 7]],
	garbageRenewal: [[1, 2, 3], [1, 2, 3], [2, 3, 4], [3, 3, 5], [3, 5, 6]],
	uraniumRenewal: [[1, 1, 1], [1, 1, 1], [1, 2, 2], [2, 3, 2], [2, 3, 3]],

	regions: [3, 3, 4, 5, 5],
	winTrigger: [21, 17, 17, 15, 14]
};