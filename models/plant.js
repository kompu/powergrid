var mongoose = require('mongoose');
var plantSchema = require('./plant_schema');

module.exports = mongoose.model('Plant',plantSchema);