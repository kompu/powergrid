var mongoose = require('mongoose');

citySchema = mongoose.Schema({
	id: Number,
    name: String,
    zone: Number,
    connections: [{
        target: Number,
        cost: Number
    }]
});

module.exports = citySchema;