var mongoose = require('mongoose');
var consts = require('./consts');

plantSchema = mongoose.Schema({
    cost: Number,
    requirement: Number,
    power: Number,
    plantType: { type: String, enum: consts.plantTypes },
    image: String
});

plantSchema.methods.acceptsResource = function(resource) {
	if (this.plantType == consts.HYBRID) {
		return resource == consts.COAL || resource == consts.OIL;
	} else if (this.plantType == consts.NUCLEAR) {
        return resource == consts.URANIUM;
    } else {
		return this.plantType == resource;
	}
}

module.exports = plantSchema;