var mongoose = require('mongoose');
var consts = require('./consts');
var mapSchema = require('./map_schema');
var plantSchema = require('./plant_schema');

var gameSchema = mongoose.Schema({
    name: String,
    map: mapSchema,
    regions: [Number],
    players: [{ 
        user: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
        order: Number,
        money: Number,
        plants: [{
            plant: plantSchema,
            coal: Number,
            oil: Number,
            garbage: Number,
            uranium: Number
        }],
        cities: [Number],
        marker: String
    }],
    auction: {
        target: plantSchema,
        stake: Number,
        biddingPlayers: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }],
        validPlayers: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }],
        shouldAdvance: Boolean
    },
    buyingResources: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }],
    buyingCities: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }],
    poweringCities: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }],
    phase: Number,
    state: { type: String, enum: consts.gameStates },
    stack: [plantSchema],
    bottomStack: [plantSchema],
    coal: Number,
    oil: Number,
    garbage: Number,
    uranium: Number,
    round: Number,
    lastPowering: [{
        user: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
        poweredCities: Number
    }]
});

function shuffle(o, offset) {
    for(var j, x, i = o.length - offset; i; j = Math.floor(Math.random() * i), x = o[--i + offset], o[i + offset] = o[j + offset], o[j + offset] = x);
    return o;
}

function arrayContainsPlant(array, plant) {
    for (var i = 0; i < array.length; ++i) {
        if (array[i].cost == plant.cost) {
            return i;
        }
    }
    return -1;
}

function arrayContainsPlayerPlant(array, plant) {
    for (var i = 0; i < array.length; ++i) {
        if (array[i].plant.cost == plant.plant.cost) {
            return i;
        }
    }
    return -1;
}

function validPowerPlan(player, plan) {
    var owned = [];
    var plantMap = {};
    for (var plant of player.plants) {
        owned.push(plant.plant.cost);
        plantMap[plant.plant.cost] = plant;
    }
    for (var it of plan) {
        var index = owned.indexOf(it.plant);
        if (index == -1) {
            return false;
        }
    }
    for (var it of plan) {
        var plant = plantMap[it.plant];
        if (it.coal > plant.coal
            || it.oil > plant.oil
            || it.garbage > plant.garbage
            || it.uranium > plant.uranium) {
            return false;
        }
        var sum = it.coal + it.oil + it.garbage + it.uranium;
        if (sum != plant.plant.requirement) {
            return false;
        }
    }
    return true;
}

function performPowerPlan(player, plan) {
    var plantMap = {};
    for (var plant of player.plants) {
        plantMap[plant.plant.cost] = plant;
    }
    var powered = 0;
    for (var it of plan) {
        var plant = plantMap[it.plant];
        plant.coal -= it.coal;
        plant.oil -= it.oil;
        plant.garbage -= it.garbage;
        plant.uranium -= it.uranium;
        powered += plant.plant.power;
    }
    return powered;
}

function getPlantsResource(plant, resource) {
    switch (resource) {
        case consts.COAL:
            return plant.coal;
        case consts.OIL:
            return plant.oil;
        case consts.GARBAGE:
            return plant.garbage;
        case consts.URANIUM:
            return plant.uranium;
        default:
            return -1;
    }
}

function getPlantsCapacityLeft(plant) {
    switch (plant.plant.plantType) {
        case consts.COAL:
            return 2 * plant.plant.requirement - plant.coal;
        case consts.OIL:
            return 2 * plant.plant.requirement - plant.oil;
        case consts.GARBAGE:
            return 2 * plant.plant.requirement - plant.garbage;
        case consts.HYBRID:
            return 2 * plant.plant.requirement - plant.coal - plant.oil;
        case consts.NUCLEAR:
            return 2 * plant.plant.requirement - plant.uranium;
    }
}

function setPlantsResource(plant, resource, value) {
    switch (resource) {
        case consts.COAL:
            plant.coal = value;
            break;
        case consts.OIL:
            plant.oil = value;
            break;
        case consts.GARBAGE:
            plant.garbage = value;
            break;
        case consts.URANIUM:
            plant.uranium = value;
            break;
    }
}

gameSchema.methods.initGame = function(lobby) {
    this.name = lobby.name;
    this.map = lobby.map;

    this.phase = 1;
    this.state = consts.AUCTION;
    this.coal = 24;
    this.oil = 18;
    this.garbage = 6;
    this.uranium = 2;
    this.round = 1;

    var markers = ['/markers/red.png', '/markers/blue.png', '/markers/green.png', '/markers/yellow.png', '/markers/cyan.png', '/markers/pink.png'];
    var order = 0;
    for (var user of lobby.players) {
        this.players.push({
            user: user,
            order: order,
            money: 50,
            plants: [],
            cities: [],
            marker: markers[this.players.length]
        });
        order += 1;
    }

    this.stack = this.map.plants.slice(0);
    this.stack.sort(function(a, b) {
        return a.cost - b.cost
    });
    
    var tmp = this.stack[8];
    this.stack[8] = this.stack[10];
    this.stack[10] = tmp;

    shuffle(this.stack, 9);
    var plantsToRemove = consts.removePlants[this.players.length - 2];
    if (plantsToRemove != 0) {
        this.stack.splice(this.stack.length - plantsToRemove, plantsToRemove);
    }

    this.buyingResources = [];
    this.buyingCities = [];
    this.regions = [];
    this.state = consts.REGIONS;
}

gameSchema.methods.availableRegions = function() {
    var regions = [];
    if (this.regions.length == 0) {
        for (var region of this.map.regions) {
            regions.push(region.id);
        }
        return regions;
    } else {
        for (var region of this.map.regions) {
            var index = this.regions.indexOf(region.id);
            if (index == -1) {
                for (var it of region.connections) {
                    index = this.regions.indexOf(it);
                    if (index != -1) {
                        regions.push(region.id);
                        break;
                    }
                }
            }
        }
        return regions;
    }
}

gameSchema.methods.chooseRegion = function(userId, region) {
    if (this.state == consts.REGIONS) {
        var availableRegions = this.availableRegions();
        var order = this.players[this.regions.length % this.players.length].user;
        if (order.equals(userId) && availableRegions.indexOf(region) != -1) {
            this.regions.push(region);
            if (this.regions.length == consts.regions[this.players.length - 2]) {
                this.startAuctionState();
            }
        }
    }
}

gameSchema.methods.isCityOpened = function(cityId) {
    var city;
    for (var it of this.map.cities) {
        if (it.id == cityId) {
            city = it;
            break;
        }
    }
    return this.regions.indexOf(city.zone) != -1;
}

gameSchema.methods.getPlayer = function(userId) {
    for (var player of this.players) {
        if (player.user.equals(userId)) {
            return player;
        }
    }
    return null;
}

gameSchema.methods.getOrder = function() {
    var order = new Array(this.players.length);
    for (var player of this.players) {
        order[player.order] = player.user;
    }
    return order;
}

// AUCTION

gameSchema.methods.startAuctionState = function() {
    this.state = consts.AUCTION;
    this.auction.validPlayers = this.getOrder();
    this.auction.stake = -1;
    this.auction.biddingPlayers = null;
    this.auction.target = null;
    this.auction.shouldAdvance = false;
}

gameSchema.methods.getAvailablePlants = function() {
    var plants = [];
    var count = this.phase == 3 ? 6 : 4;
    count = Math.min(count, this.stack.length);
    for (var i = 0; i < count; ++i) {
        plants.push(this.stack[i]);
    }
    return plants;
}

gameSchema.methods.removePlantFromMarket = function(plant) {
    var index = arrayContainsPlant(this.stack, plant);
    if (index > -1) {
        this.stack.splice(index, 1);
        var count = this.phase == 3 ? 6 : 8;
        count = Math.min(count, this.stack.length);
        var market = this.stack.slice(0, count);
        market.sort(function(a, b) {
            return a.cost - b.cost
        });
        for (var i = 0; i < market.length; ++i) {
            this.stack[i] = market[i];
        }
    } else {
        console.log('Plant is not available: '+plant);
    }
}

gameSchema.methods.buyPlant = function(user, plant, cost) {
    var player = this.getPlayer(user._id);
    if (player.money >= cost) {
        player.money -= cost;
        var playersPlant = {
            plant: plant,
            coal: 0,
            oil: 0,
            garbage: 0,
            uranium: 0
        }
        player.plants.push(playersPlant);
        this.removePlantFromMarket(plant);
        var index = this.auction.validPlayers.indexOf(user._id);
        this.auction.validPlayers.splice(index, 1);
        this.auction.target = null;
    } else {
        console.log('Player dont have enough money, player: '+player+', plant: '+plant);
    }
}

gameSchema.methods.discardPlant = function(user, plant) {
    var player = this.getPlayer(user._id);
    var index = arrayContainsPlayerPlant(player.plants, plant);
    if (index > -1) {
        player.plants.splice(index, 1);
        if (this.state == consts.AUCTION && this.auction.validPlayers.length === 0) {
            this.startResourceState();
        }
    } else {
        console.log('Player dont own this plant: '+plant+', player: '+player);
    }
}

gameSchema.methods.pickPlantForAuction = function(user, plant) {
    var plants = this.getAvailablePlants();
    var player = this.getPlayer(user._id);
    if (this.state == consts.AUCTION 
        && this.auction.target == null
        && this.auction.validPlayers[0].equals(player.user)
        && arrayContainsPlant(plants, plant) > -1 
        && player.money >= plant.cost) {
        this.auction.target = {
            cost: plant.cost,
            requirement: plant.requirement,
            power: plant.power,
            plantType: plant.plantType,
            image: plant.image
        };
        this.auction.stake = plant.cost;
        this.auction.biddingPlayers = this.auction.validPlayers.slice(0);
        this.moveAuction();
        if (this.auction.validPlayers.length === 0) {
            this.startResourceState();
        }
    } else {
        console.log(this.auction);
        console.log('Pick plant error, player: '+player+', plant: '+plant);
    }    
}

gameSchema.methods.moveAuction = function() {
    var first = this.auction.biddingPlayers[0];
    if (this.auction.biddingPlayers.length == 1) {
        this.buyPlant({ _id: first }, this.auction.target, this.auction.stake);
    } else {
        this.auction.biddingPlayers.splice(0, 1);
        this.auction.biddingPlayers.push(first);
    }
    if (this.stack.length < 8) {
        this.auction.shouldAdvance = true;
    }
}

gameSchema.methods.bidForPlant = function(user, value) {
    var player = this.getPlayer(user._id);
    if (this.state == consts.AUCTION 
        && this.auction.target != null 
        && this.auction.biddingPlayers[0].equals(player.user)
        && this.auction.stake < value
        && player.money >= value) {
        this.auction.stake = value;
        this.moveAuction();
    }
}

gameSchema.methods.passForPlant = function(user) {
    var player = this.getPlayer(user._id);
    if (this.state == consts.AUCTION) {
        if (this.auction.target != null) {
            if (this.auction.biddingPlayers[0].equals(player.user)) {
                this.auction.biddingPlayers.splice(0, 1);
                if (this.auction.biddingPlayers.length == 1) {
                    var buyer = this.auction.biddingPlayers[0];
                    this.buyPlant({ _id: buyer }, this.auction.target, this.auction.stake);
                }
            }
        } else if (this.round > 1 && this.auction.validPlayers[0].equals(player.user)) {
            this.auction.validPlayers.splice(0, 1);
            this.auction.target = null;
            if (this.auction.validPlayers.length === 0) {
                this.startResourceState();
            }
        }
    }
}

// RESOURCES

gameSchema.methods.startResourceState = function() {
    for (var player of this.players) {
        if (player.plants.length > consts.availablePlants[this.players.length - 2]) {
            return;
        }
    }
    if (this.round == 1) {
        this.establishOrder();
    }
    if (this.auction.shouldAdvance) {
        this.advanceToThirdPhase();
        this.auction.shouldAdvance = false;
    }
    this.state = consts.RESOURCES;
    this.buyingResources = this.getOrder();
}

gameSchema.methods.moveResourceBetweenPlants = function(resource, amount, from, to) {
    var fromVal = getPlantsResource(from, resource);
    if (from.plant.acceptsResource(resource) 
        && to.plant.acceptsResource(resource)
        && fromVal >= amount
        && getPlantsCapacityLeft(to) >= amount) {
        var toVal = getPlantsResource(to, resource);
        setPlantsResource(from, resource, fromVal - amount);
        setPlantsResource(to, resource, toVal + amount);
    }
}

gameSchema.methods.resourceCost = function(resource, amount) {
    var ERR = -1;
    var uraniumCosts = [ERR, 16, 14, 12, 10, 8, 7, 6, 5, 4, 3, 2, 1];
    var otherCosts = [ERR, 8, 8, 8, 7, 7, 7, 6, 6, 6, 5, 5, 5, 4, 4, 4, 3, 3, 3, 2, 2, 2, 1, 1, 1];
    var available;
    var costs;
    switch (resource) {
        case consts.COAL:
            available = this.coal;
            costs = otherCosts;
            break;
        case consts.OIL:
            available = this.oil;
            costs = otherCosts;
            break;
        case consts.GARBAGE:
            available = this.garbage;
            costs = otherCosts;
            break;
        case consts.URANIUM:
            available = this.uranium;
            costs = uraniumCosts;
            break;
        default:
            return ERR;
    }
    if (available >= amount) {
        var sum = 0;
        for (var i = available; i > available - amount; --i) {
            sum += costs[i];
        }
        return sum;
    } else {
        return ERR;
    }
}

gameSchema.methods.buyResources = function(resource, user, plant, amount) {
    var cost = this.resourceCost(resource, amount);
    var player = this.getPlayer(user._id);
    if (this.state == consts.RESOURCES
        && this.buyingResources[this.buyingResources.length - 1].equals(user._id)
        && plant.plant.acceptsResource(resource)
        && getPlantsCapacityLeft(plant) >= amount
        && cost != -1
        && player.money >= cost) {
        player.money -= cost;
        switch (resource) {
            case consts.COAL:
                plant.coal += amount;
                this.coal -= amount;
                break;
            case consts.OIL:
                plant.oil += amount;
                this.oil -= amount;
                break;
            case consts.GARBAGE:
                plant.garbage += amount;
                this.garbage -= amount;
                break;
            case consts.URANIUM:
                plant.uranium += amount;
                this.uranium -= amount;
                break;
            default:
                player.money += cost;
                break;
        }
    }
}

gameSchema.methods.endResourceBuy = function(user) {
    var index = this.buyingResources.indexOf(user._id);
    if (index != -1) {
        this.buyingResources.splice(index, 1);
        if (this.buyingResources.length == 0) {
            this.startCitiesState();
        }
    }
}

// CITIES

gameSchema.methods.startCitiesState = function() {
    this.state = consts.STATIONS;
    this.buyingCities = this.getOrder();
}

gameSchema.methods.getCity = function(cityId) {
    for (city of this.map.cities) {
        if (city.id == cityId) {
            return city;
        }
    }
}

gameSchema.methods.getCityByName = function(cityName) {
    for (city of this.map.cities) {
        if (city.name == cityName) {
            return city;
        }
    }
}

gameSchema.methods.getConnectionCost = function(from, to) {
    var city = this.getCity(from);
    for (it of city.connections) {
        if (it.target == to) {
            return it.cost;
        }
    }
    return -1;
}

gameSchema.methods.getCityBuyingCosts = function() {
    var presentPlayers = new Array(this.map.cities.length);
    for (var i = 0; i < presentPlayers.length; ++i) {
        presentPlayers[i] = 0;
    }
    for (player of this.players) {
        for (it of player.cities) {
            ++presentPlayers[it];
        }
    }
    for (var i = 0; i < presentPlayers.length; ++i) {
        if (presentPlayers[i] >= this.phase || !this.isCityOpened(i)) {
            presentPlayers[i] = -1;
        } else {
            switch (presentPlayers[i]) {
                case 0:
                    presentPlayers[i] = 10;
                    break;
                case 1: 
                    presentPlayers[i] = 15;
                    break;
                case 2:
                    presentPlayers[i] = 20;
                    break;
                default:
                    presentPlayers[i] = -1;
                    break;
            }
        }
    }
    return presentPlayers;
}

gameSchema.methods.getCostsList = function(ownedCities) {
    var costs = new Array(this.map.cities.length);
    if (ownedCities.length > 0) {
        var priorityQueue = [];
        for (var i = 0; i < this.map.cities.length; ++i) {
            if (this.isCityOpened(i)) {
                costs[i] = 1000000;
                priorityQueue.push(i);
            } else {
                costs[i] = -1;
            }
        }
        for (var owned of ownedCities) {
            costs[owned] = 0;
        }
        while (priorityQueue.length > 0) {
            var min = 0;
            for (var i = 1; i < priorityQueue.length; ++i) {
                if (costs[priorityQueue[i]] < costs[priorityQueue[min]]) {
                    min = i;
                }
            }
            var cityId = priorityQueue.splice(min, 1);

            var city = this.getCity(cityId);
            for (it of city.connections) {
                if (costs[it.target] != -1 && costs[it.target] > costs[city.id] + it.cost) {
                    costs[it.target] = costs[city.id] + it.cost;
                }
            }
        }
    } else {
        for (var i = 0; i < costs.length; ++i) {
            costs[i] = 0;
        }
    }
    
    var cityCosts = this.getCityBuyingCosts();
    for (var i = 0; i < costs.length; ++i) {
        if (cityCosts[i] == -1) {
            costs[i] = -1;
        } else {
            costs[i] += cityCosts[i];
        }
    }
    for (var it of ownedCities) {
        costs[it] = -1;
    }
    return costs;
}

gameSchema.methods.buyCity = function(user, cityId) {
    var player = this.getPlayer(user._id);
    var costs = this.getCostsList(player.cities);
    if (this.state == consts.STATIONS
        && this.buyingCities[this.buyingCities.length - 1].equals(user._id)
        && this.isCityOpened(cityId)
        && player.cities.indexOf(cityId) == -1
        && costs[cityId] != -1
        && player.money >= costs[cityId]) {
        player.money -= costs[cityId];
        player.cities.push(cityId);
    }
}

gameSchema.methods.buyCitiesForUser = function(user, cityIds) {
    if (this.buyingCities[this.buyingCities.length - 1].equals(user._id)) {
        for (var cityId of cityIds) {
            this.buyCity(user, cityId);
        }
        this.checkAndRemoveCheapPlants();
        if (this.stack.length < 8 && this.phase < 3) {
            this.advanceToThirdPhase();
        }
        this.buyingCities.splice(this.buyingCities.length - 1, 1);
        if (this.buyingCities.length == 0) {
            this.startBureaucracyState();
        }
    }
}

// BUREAUCRACY

gameSchema.methods.startBureaucracyState = function() {
    if (this.phase == 1) {
        for (var player of this.players) {
            if (player.cities.length >= consts.requiredCities[this.players.length - 2]) {
                this.phase = 2;
                if (this.stack.length > 0) {
                    this.removePlantFromMarket(this.stack[0]);
                }
            }
        }
    }

    this.state = consts.BUREAUCRACY;
    this.poweringCities = this.getOrder();
    this.lastPowering = [];
}

gameSchema.methods.powerCities = function(user, plan) {
    var index = this.poweringCities.indexOf(user._id);
    var player = this.getPlayer(user._id);
    if (index != -1 && validPowerPlan(player, plan)) {
        this.poweringCities.splice(index, 1);
        var powered = performPowerPlan(player, plan);
        player.money += consts.powerPays[Math.min(powered, player.cities.length)];
        this.lastPowering.push({
            user: user._id,
            poweredCities: powered
        });

        if (this.poweringCities.length == 0) {
            this.endPhase();
        }
    }
}

gameSchema.methods.establishOrder = function() {
    var players = this.players.slice(0);
    players.sort(function(a, b) {
        if (a.cities.length == b.cities.length) {
            var first = 0;
            for (var plant of a.plants) {
                first = Math.max(first, plant.plant.cost);
            }
            var second = 0;
            for (var plant of b.plants) {
                second = Math.max(second, plant.plant.cost);
            }
            return second - first;
        } else {
            return b.cities.length - a.cities.length;
        }
    });
    var orderMap = {};
    for (var i = 0; i < players.length; ++i) {
        orderMap[players[i].user] = i;
    }
    for (var player of this.players) {
        player.order = orderMap[player.user];
    }
}

gameSchema.methods.getRenewal = function(resource) {
    switch(resource) {
        case consts.COAL:
            return consts.coalRenewal[this.players.length - 2][this.phase - 1];
        case consts.OIL:
            return consts.oilRenewal[this.players.length - 2][this.phase - 1];
        case consts.GARBAGE:
            return consts.garbageRenewal[this.players.length - 2][this.phase - 1];
        case consts.URANIUM:
            return consts.uraniumRenewal[this.players.length - 2][this.phase - 1];
    }
}

gameSchema.methods.endPhase = function() {
    for (var player of this.players) {
        if (player.cities.length >= consts.winTrigger[this.players.length - 2]) {
            this.state = consts.END;
            return;
        }
    }
    if (this.phase == 3) {
        if (this.stack.length > 0) {
            this.removePlantFromMarket(this.stack[0]);
        }
    } else {
        var plant = this.stack[7];
        this.removePlantFromMarket(plant);
        this.bottomStack.push(plant);
        if (this.stack.length < 8) {
            this.advanceToThirdPhase();
        }
    }
    this.coal = Math.min(consts.MAX_RESOURCES, this.coal + this.getRenewal(consts.COAL));
    this.oil = Math.min(consts.MAX_RESOURCES, this.oil + this.getRenewal(consts.OIL));
    this.garbage = Math.min(consts.MAX_RESOURCES, this.garbage + this.getRenewal(consts.GARBAGE));
    this.uranium = Math.min(consts.MAX_URANIUM, this.uranium + this.getRenewal(consts.URANIUM));

    this.establishOrder();
    this.round += 1;
    this.startAuctionState();
}

gameSchema.methods.advanceToThirdPhase = function() {
    this.phase = 3;
    shuffle(this.bottomStack, 0);
    for (var plant of this.bottomStack) {
        this.stack.push(plant);
    }
    this.bottomStack = [];
    this.removePlantFromMarket(this.stack[0]);
}

gameSchema.methods.removeCheapPlants = function(price) {
    var removeCheaper = function(plants) {
        var i = 0;
        while (i < plants.length) {
            if (plants[i].cost <= price) {
                plants.splice(i, 1);
            } else {
                i += 1;
            }
        }
    }
    removeCheaper(this.stack);
    removeCheaper(this.bottomStack);
}

gameSchema.methods.checkAndRemoveCheapPlants = function() {
    var maxCities = 0;
    for (var player of this.players) {
        maxCities = Math.max(maxCities, player.cities.length);
    }
    this.removeCheapPlants(maxCities);
}

gameSchema.methods.whoWon = function() {
    if (this.state != consts.END) {
        return null;
    } else {
        for (var it of this.lastPowering) {
            it.player = this.getPlayer(it.user);
        }
        this.lastPowering.sort(function(a, b) {
            if (a.poweredCities == b.poweredCities) {
                if (a.player.money == b.player.money) {
                    return b.player.cities.length - a.player.cities.length;
                } else {
                    return b.player.money - a.player.money;
                }
            } else {
                return b.poweredCities - a.poweredCities;
            }
        });
        return this.lastPowering[0].user;
    }
}

module.exports = gameSchema;