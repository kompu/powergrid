var mongoose = require('mongoose');
var lobbySchema = require('./lobby_schema');

module.exports = mongoose.model('Lobby',lobbySchema);