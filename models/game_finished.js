var mongoose = require('mongoose');
var gameSchema = require('./game_schema');

module.exports = mongoose.model('FinishedGame',gameSchema);