'use strict';

angular.module('powergrid')
.controller('MapController', function ($scope) {

    $scope.playersInCities = [];
    $scope.$watch('game', function(newValue, oldValue) {
        $scope.playersInCities = [];
        for (var i of $scope.game.map.cities) {
            $scope.playersInCities.push([]);
        }
        for (var player of $scope.game.players) {
            for (var city of player.cities) {
                $scope.playersInCities[city].push(player);
            }
        }
    });

    $scope.repeat = function(n) {
        var ret = [];
        for (var i = 1; i <= n; ++i) {
            ret.push(i);
        }
        return ret;
    }
});