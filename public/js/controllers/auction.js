'use strict';

angular.module('powergrid')
.controller('AuctionController', function ($scope, socket, utils) {

    $scope.bidAmount = $scope.game.auction.stake + 1;

    $scope.$on('game', function() {
        console.log('game');
        $scope.bidAmount = $scope.game.auction.stake + 1;
    });

    $scope.bidForPlant = function (amount) {
        if (amount) {
            socket.emit('bid-plant', amount);
        }
    };

    $scope.passForPlant = function() {
        socket.emit('bid-plant', -1);
    }

    $scope.maxPlants = function() {
        return utils.maxPlants($scope.game);
    }

    $scope.toManyPlants = function() {
        return $scope.myPlayer.plants.length > utils.maxPlants($scope.game);
    }
});