'use strict';

angular.module('powergrid')
.controller('ModalPlayerCtrl', function ($scope, $rootScope, $uibModalInstance, socket, playerIndex, utils) {

    $scope.movePlantColapsed = true;
    $scope.selectedPlant = -1;
    $scope.plantClasses = [];
    $scope.playerIndex = playerIndex;
    $rootScope.$watch('game', function(newValue, oldValue) {
        var newPlayer = $rootScope.game.players[playerIndex];
        if (!utils.comparePlayerPlants($scope.player, newPlayer)) {
            $scope.player = newPlayer;
        }
    });
    $scope.player = $rootScope.game.players[playerIndex];
    $scope.authorized = $scope.player.user == $rootScope.myId;

    $scope.buy = {
        coal: 0,
        oil: 0,
        garbage: 0,
        uranium: 0
    }
    $scope.move = {
        coal: 0,
        oil: 0,
        garbage: 0,
        uranium: 0
    }
    $scope.$watchCollection('buy', function(newValue, oldValue) {
        var coal = utils.resourceCost($scope.game, 'coal', $scope.buy.coal);
        var oil = utils.resourceCost($scope.game, 'oil', $scope.buy.oil);
        var garbage = utils.resourceCost($scope.game, 'garbage', $scope.buy.garbage);
        var uranium = utils.resourceCost($scope.game, 'uranium', $scope.buy.uranium);
        $scope.buyCost = coal + oil + garbage + uranium;
    });
    $scope.$watch('moveFromPlant', function(newValue, oldValue) {
        $scope.move = {
            coal: 0,
            oil: 0,
            garbage: 0,
            uranium: 0
        }
    });

    for (var plant of $scope.player.plants) {
        $scope.plantClasses.push('plant-anchor');
    }

    $scope.close = function () {
        $uibModalInstance.close();
    };

    $scope.showBureaucracy = function() {
        return $rootScope.game.state=='bureaucracy' && $scope.authorized && $rootScope.game.poweringCities.indexOf($scope.player.user) != -1;
    }

    $scope.selectPlant = function(plant) {
        if ($scope.authorized) {
            if ($scope.selectedPlant != -1) {
                $scope.plantClasses[$scope.selectedPlant] = 'plant-anchor';
            }
            $scope.selectedPlant = $scope.player.plants.indexOf(plant);
            $scope.plantClasses[$scope.selectedPlant] = 'plant-anchor plant-selected';
            $scope.buy = {
                coal: 0,
                oil: 0,
                garbage: 0,
                uranium: 0
            }
        }
    }

    $scope.buyResources = function () {
        if ($rootScope.game.auction.buyingResources[0].user == $rootScope.myId) {
            socket.emit('buy-resources', $scope.buy);
        }
    }

    $scope.acceptsResource = function(plant, resource) {
        return utils.acceptsResource(plant, resource);
    }

    $scope.canPower = function(plant) {
        return utils.canPower(plant);
    }

    $scope.income = function() {
        var powered = 0;
        for (var plant of $scope.player.plants) {
            if (utils.canPower(plant)) {
                powered += plant.plant.power;
            }
        }
        return utils.incomeForPower(Math.min(powered, $scope.player.cities.length));
    }

    $scope.powerCities = function() {
        var plan = [];
        for (var plant of $scope.player.plants) {
            var coal = plant.buyCoal === undefined ? 0 : plant.buyCoal;
            var oil = plant.buyOil === undefined ? 0 : plant.buyOil;
            var garbage = plant.buyGarbage === undefined ? 0 : plant.buyGarbage;
            var uranium = plant.buyUranium === undefined ? 0 : plant.buyUranium;
            if (utils.canPower(plant)) {
                plan.push({
                    plant: plant.plant.cost,
                    coal: coal,
                    oil: oil,
                    garbage: garbage,
                    uranium: uranium
                });
            }
        }
        socket.emit('power-plan', plan);
        $uibModalInstance.close();
    }

    $scope.canMoveResources = function() {
        if ($scope.moveFromPlant == $scope.moveToPlant || $scope.moveFromPlant === undefined || $scope.moveToPlant === undefined) {
            return {
                any: false
            }
        } else {
            var moveFromPlant = $scope.player.plants[$scope.moveFromPlant].plant;
            var moveToPlant = $scope.player.plants[$scope.moveToPlant].plant;
            var coal = utils.acceptsResource(moveFromPlant, 'coal') && utils.acceptsResource(moveToPlant, 'coal');
            var oil = utils.acceptsResource(moveFromPlant, 'oil') && utils.acceptsResource(moveToPlant, 'oil');
            var garbage = utils.acceptsResource(moveFromPlant, 'garbage') && utils.acceptsResource(moveToPlant, 'garbage');
            var uranium = utils.acceptsResource(moveFromPlant, 'uranium') && utils.acceptsResource(moveToPlant, 'uranium');
            var any = coal || oil || garbage || uranium;
            var ret = {
                any: any,
                coal: coal,
                garbage: garbage,
                oil: oil,
                uranium: uranium
            }
            return ret;
        }
    }

    $scope.moveResources = function() {
        if ($scope.move.coal != -1) {
            socket.emit('move-resources', {
                resource: 'coal',
                amount: $scope.move.coal,
                from: $scope.moveFromPlant,
                to: $scope.moveToPlant
            });
        }
        if ($scope.move.oil != -1) {
            socket.emit('move-resources', {
                resource: 'oil',
                amount: $scope.move.oil,
                from: $scope.moveFromPlant,
                to: $scope.moveToPlant
            });
        }
        if ($scope.move.garbage != -1) {
            socket.emit('move-resources', {
                resource: 'garbage',
                amount: $scope.move.garbage,
                from: $scope.moveFromPlant,
                to: $scope.moveToPlant
            });
        }
        if ($scope.move.uranium != -1) {
            socket.emit('move-resources', {
                resource: 'uranium',
                amount: $scope.move.uranium,
                from: $scope.moveFromPlant,
                to: $scope.moveToPlant
            });
        }
        $scope.move = {
            coal: 0,
            oil: 0,
            garbage: 0,
            uranium: 0
        }
    }

    $scope.canBuyResources = function() {
        var plant = $scope.player.plants[$scope.selectedPlant].plant;
        var coal = utils.acceptsResource(plant, 'coal');
        var oil = utils.acceptsResource(plant, 'oil');
        var garbage = utils.acceptsResource(plant, 'garbage');
        var uranium = utils.acceptsResource(plant, 'uranium');
        var ret = {
            coal: coal,
            garbage: garbage,
            oil: oil,
            uranium: uranium
        }
        return ret;
    }

    $scope.buyResources = function() {
        if ($scope.buy.coal != -1) {
            socket.emit('buy-resources', {
                resource: 'coal',
                plant: $scope.selectedPlant,
                amount: $scope.buy.coal
            });
        }
        if ($scope.buy.oil != -1) {
            socket.emit('buy-resources', {
                resource: 'oil',
                plant: $scope.selectedPlant,
                amount: $scope.buy.oil
            });
        }
        if ($scope.buy.garbage != -1) {
            socket.emit('buy-resources', {
                resource: 'garbage',
                plant: $scope.selectedPlant,
                amount: $scope.buy.garbage
            });
        }
        if ($scope.buy.uranium != -1) {
            socket.emit('buy-resources', {
                resource: 'uranium',
                plant: $scope.selectedPlant,
                amount: $scope.buy.uranium
            });
        }
    }

    $scope.dropPlant = function(plant) {
        socket.emit('drop-plant', plant);
    }

    $scope.resourcesForPlant = function(plant) {
        if (plant.resources === undefined || plant.coal + plant.oil + plant.garbage + plant.uranium != plant.resources.length) {
            plant.resources = [];
            for (var i = 0; i < plant.coal; ++i) {
                plant.resources.push({
                    css: 'res-' + plant.resources.length,
                    image: '/markers/coal.png'
                });
            }
            for (var i = 0; i < plant.oil; ++i) {
                plant.resources.push({
                    css: 'res-' + plant.resources.length,
                    image: '/markers/oil.png'
                });
            }
            for (var i = 0; i < plant.garbage; ++i) {
                plant.resources.push({
                    css: 'res-' + plant.resources.length,
                    image: '/markers/garbage.png'
                });
            }
            for (var i = 0; i < plant.uranium; ++i) {
                plant.resources.push({
                    css: 'res-' + plant.resources.length,
                    image: '/markers/uranium.png'
                });
            }
        }
        return plant.resources;
    }
});