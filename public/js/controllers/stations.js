'use strict';

angular.module('powergrid')
.controller('StationsController', function ($scope, socket, utils, $log) {

    $scope.checkedCities = [];
    $scope.availableCities = cities;
    $scope.cost = 0;

    var cities = [];
    for (var city of $scope.game.map.cities) {
        cities.push({
            id: city.id,
            name: city.name,
            cost: -1
        });
    }

    var updateCitiesCosts = function () {
        var ownedCities = angular.copy($scope.myPlayer.cities);
        for (var city of $scope.checkedCities) {
            ownedCities.push(city.id);
        }
        var costs = utils.getCostsList($scope.game, ownedCities);
        $scope.availableCities = [];
        for (var i = 0; i < costs.length; ++i) {
            if (costs[i] != -1) {
                $scope.availableCities.push({
                    id: i,
                    name: cities[i].name,
                    cost: costs[i]
                })
            }
        }
        $scope.availableCities.sort(function(a, b) {
            return a.cost != b.cost ? a.cost - b.cost : a.name.localeCompare(b.name);
        });
        $scope.cost = 0;
        for (var city of $scope.checkedCities) {
            $scope.cost += city.cost;
        }
    }
    updateCitiesCosts();

    $scope.$on('game', function() {
        if ($scope.game.buyingCities[$scope.game.buyingCities.length - 1] == $scope.myId) {
            updateCitiesCosts();
        }
    });

    $scope.checkCity = function (index) {
        var city = $scope.availableCities[index];
        $scope.availableCities.splice(index, 1);
        $scope.checkedCities.push(city);

        updateCitiesCosts();
    }

    $scope.uncheckCity = function (index) {
        var city = $scope.checkedCities[index];
        $scope.checkedCities.splice(index, 1);
        $scope.availableCities.push(city);

        var ownedCities = angular.copy($scope.myPlayer.cities);
        for (var city of $scope.checkedCities) {
            var costs = utils.getCostsList($scope.game, ownedCities);
            city.cost = costs[city.id];
            ownedCities.push(city.id);
        }
        updateCitiesCosts();
    }

    $scope.buyStations = function () {
        if ($scope.game.buyingCities[$scope.game.buyingCities.length - 1] == $scope.myId) {
            var order = [];
            for (var city of $scope.checkedCities) {
                order.push(city.id);
            }
            socket.emit('buy-stations', order);
        }
    }
});