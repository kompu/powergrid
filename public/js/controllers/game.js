'use strict';

angular.module('powergrid')
.controller('GameController', function ($scope, $rootScope, $uibModal, socket, utils) {

    $scope.availableRegions = [];
    // $scope.pickRegion = '0';

    $scope.data = {
    };

    socket.on('init', function (data) {
        $rootScope.myId = data.userId;
    });

    socket.on('users', function (users) {
        $scope.users = users;
        if ($scope.game) {
            for (var player of $scope.game.players) {
                player.username = users[player.user].username;
                player.active = users[player.user].active;
            }
        }
    });

    socket.on('update', function (game) {
        for (var it of game.players) {
            if (it.user == $scope.myId) {
                $rootScope.myPlayer = it;
            }
        }
        if ($scope.users) {
            for (var player of game.players) {
                player.username = $scope.users[player.user].username;
                player.active = $scope.users[player.user].active;
            }
        }
        game.players.sort(function(a, b) {
            return a.order - b.order;
        });
        if (game.state == 'regions') {
            $scope.availableRegions = utils.getAvailableRegions(game);
            $scope.data.pickRegion = $scope.availableRegions[0];
        }
        if (utils.shouldPlayNotification($scope.game, game, $scope.myId)) {
            var snd = new Audio('/sounds/notification.mp3');
            snd.play();
        }
        $rootScope.game = game;
        $rootScope.$broadcast('game');
    });

    socket.on('winner', function (winner) {
        for (var player of $scope.game.players) {
            if (player.user == winner) {
                $scope.winner = player;
            }
        }
    });

    $scope.getPlayer = function(playerId) {
        for (var player of $scope.game.players) {
            if (player.user == playerId) {
                return player;
            }
        }
    }

    $scope.playerMaxPower = function(player) {
        var power = 0;
        for (var plant of player.plants) {
            power += plant.plant.power;
        }
        return power;
    }

    $scope.open = function (playerIndex) {
        if (playerIndex === undefined) {
            for (var i = 0; i < $scope.game.players.length; ++i) {
                if ($scope.game.players[i].user == $scope.myId) {
                    playerIndex = i;
                    break;
                }
            }
        }
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: '/js/directives/player.html',
            controller: 'ModalPlayerCtrl',
            size: 'lg',
            resolve: {
                playerIndex: playerIndex,
                parentScope: $scope
            }
        });
    };

    $scope.chooseRegion = function() {
        var region = $scope.data.pickRegion;
        socket.emit('region', region.id);
    }

    $scope.getRegionName = function(regionId) {
        for (var region of $scope.game.map.regions) {
            if (region.id == regionId) {
                return region.name;
            }
        }
    }

    $scope.pickPlant = function (plantChoice)  {
        if ($scope.game.auction.target == null 
            && $scope.game.auction.validPlayers[0] == $scope.myId) {
            socket.emit('pick-plant', plantChoice);
        }
    };

    $scope.endResources = function() {
        if ($scope.game.buyingResources[$scope.game.buyingResources.length - 1] == $scope.myId) {
            socket.emit('end-resources');
        }
    };
});