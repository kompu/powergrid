'use strict';

angular.module('powergrid')
.factory('utils', function () {
    var resourceCost = function (game, resource, amount) {
        var ERR = -1;
        var uraniumCosts = [ERR, 16, 14, 12, 10, 8, 7, 6, 5, 4, 3, 2, 1];
        var otherCosts = [ERR, 8, 8, 8, 7, 7, 7, 6, 6, 6, 5, 5, 5, 4, 4, 4, 3, 3, 3, 2, 2, 2, 1, 1, 1];
        var available;
        var costs;
        switch (resource) {
            case 'coal':
                available = game.coal;
                costs = otherCosts;
                break;
            case 'oil':
                available = game.oil;
                costs = otherCosts;
                break;
            case 'garbage':
                available = game.garbage;
                costs = otherCosts;
                break;
            case 'uranium':
                available = game.uranium;
                costs = uraniumCosts;
                break;
            default:
                return ERR;
        }
        if (available >= amount) {
            var sum = 0;
            for (var i = available; i > available - amount; --i) {
                sum += costs[i];
            }
            return sum;
        } else {
            return ERR;
        }
    };

    var getCity = function(game, cityId) {
        for (var city of game.map.cities) {
            if (city.id == cityId) {
                return city;
            }
        }
    }

    var isCityOpened = function(game, cityId) {
        var city;
        for (var it of game.map.cities) {
            if (it.id == cityId) {
                city = it;
                break;
            }
        }
        return game.regions.indexOf(city.zone) != -1;
    }

    var getCityBuyingCosts = function(game) {
        var presentPlayers = new Array(game.map.cities.length);
        for (var i = 0; i < presentPlayers.length; ++i) {
            presentPlayers[i] = 0;
        }
        for (var player of game.players) {
            for (var it of player.cities) {
                ++presentPlayers[it];
            }
        }
        for (var i = 0; i < presentPlayers.length; ++i) {
            if (presentPlayers[i] >= game.phase || !isCityOpened(game, i)) {
                presentPlayers[i] = -1;
            } else {
                switch (presentPlayers[i]) {
                    case 0:
                        presentPlayers[i] = 10;
                        break;
                    case 1: 
                        presentPlayers[i] = 15;
                        break;
                    case 2:
                        presentPlayers[i] = 20;
                        break;
                    default:
                        presentPlayers[i] = -1;
                        break;
                }
            }
        }
        return presentPlayers;
    }

    var getCostsList = function(game, ownedCities) {
        var costs = new Array(game.map.cities.length);
        if (ownedCities.length > 0) {
            var priorityQueue = [];
            for (var i = 0; i < game.map.cities.length; ++i) {
                costs[i] = 1000000;
                if (isCityOpened(game, i)) {
                    costs[i] = 1000000;
                    priorityQueue.push(i);
                } else {
                    costs[i] = -1;
                }
            }
            for (var owned of ownedCities) {
                costs[owned] = 0;
            }
            while (priorityQueue.length > 0) {
                var min = 0;
                for (var i = 1; i < priorityQueue.length; ++i) {
                    if (costs[priorityQueue[i]] < costs[priorityQueue[min]]) {
                        min = i;
                    }
                }
                var cityId = priorityQueue.splice(min, 1);

                var city = getCity(game, cityId);
                for (var it of city.connections) {
                    if (costs[it.target] != -1 && costs[it.target] > costs[city.id] + it.cost) {
                        costs[it.target] = costs[city.id] + it.cost;
                    }
                }
            }
        } else {
            for (var i = 0; i < costs.length; ++i) {
                costs[i] = 0;
            }
        }
        
        var cityCosts = getCityBuyingCosts(game);
        for (var i = 0; i < costs.length; ++i) {
            if (cityCosts[i] == -1) {
                costs[i] = -1;
            } else {
                costs[i] += cityCosts[i];
            }
        }
        for (var it of ownedCities) {
            costs[it] = -1;
        }
        return costs;
    }

    var acceptsResource = function(plant, resource) {
        if (plant.plantType == 'hybrid') {
            return resource == 'coal' || resource == 'oil';
        } else if (plant.plantType == 'nuclear') {
            return resource == 'uranium';
        } else {
            return plant.plantType == resource;
        }
    }

    var canPower = function(plant) {
        var supplied = 0;
        switch(plant.plant.plantType) {
            case 'coal':
                supplied += plant.buyCoal;
                break;
            case 'oil':
                supplied += plant.buyOil;
                break;
            case 'hybrid':
                supplied += plant.buyCoal + plant.buyOil;
                break;
            case 'garbage':
                supplied += plant.buyGarbage;
                break;
            case 'nuclear':
                supplied += plant.buyUranium;
                break;
        }
        return plant.plant.requirement == supplied;
    }

    var incomeForPower = function(powered) {
        var powerPays = [10, 22, 33, 44, 54, 64, 73, 82, 90, 98, 105, 112, 118, 124, 129, 134, 138, 142, 145, 148, 150];
        return powerPays[powered];
    }

    var maxPlants = function(game) {
        if (game.players.length == 2) {
            return 4;
        } else {
            return 3;
        }
    }

    var getAvailableRegions = function(game) {
        var regions = [];
        if (game.regions.length == 0) {
            return game.map.regions.slice(0);
        } else {
            for (var region of game.map.regions) {
                var index = game.regions.indexOf(region.id);
                if (index == -1) {
                    for (var it of region.connections) {
                        index = game.regions.indexOf(it);
                        if (index != -1) {
                            regions.push(region);
                            break;
                        }
                    }
                }
            }
            return regions.slice(0);
        }
    }

    var comparePlayerPlants = function(player1, player2) {
        if (player1.user != player2.user) {
            return false;
        }
        if (player1.money != player2.money) {
            return false;
        }
        if (player1.plants.length != player2.plants.length) {
            return false;
        }
        var comparePlant = function(plant1, plant2) {
            return plant1.coal == plant2.coal &&
                plant1.oil == plant2.oil &&
                plant1.garbage == plant2.garbage &&
                plant1.uranium == plant2.uranium &&
                plant1.plant.cost == plant2.plant.cost;
        }
        for (var i = 0; i < player1.plants.length; ++i) {
            if (!comparePlant(player1.plants[i], player2.plants[i])) {
                return false;
            }
        }
        return true;
    }

    var shouldPlayNotification = function(oldGame, newGame, myId) {
        if (oldGame === undefined) {
            return false;
        }
        if (oldGame.state == 'stations' && newGame.state == 'bureaucracy') {
            return true;
        }
        if (oldGame.state == 'bureaucracy' && newGame.state == 'auction') {
            return true;
        }
        if (oldGame.auction != null && oldGame.auction.biddingPlayers != null && newGame.auction.biddingPlayers != null && oldGame.auction.biddingPlayers[0] != newGame.auction.biddingPlayers[0] && newGame.auction.biddingPlayers[0] == myId) {
            return true;
        }
        if (oldGame.auction != null && newGame.auction.target == null && oldGame.auction.validPlayers != null && newGame.auction.validPlayers != null && oldGame.auction.validPlayers[0] != newGame.auction.validPlayers[0] && newGame.auction.validPlayers[0] == myId) {
            return true;
        }
        if (oldGame.buyingResources[0] != newGame.buyingResources[0] && newGame.buyingResources[0] == myId) {
            return true;
        }
        if (oldGame.buyingCities[0] != newGame.buyingCities[0] && newGame.buyingCities[0] == myId) {
            return true;
        }
        return false;
    }

    return {
        resourceCost: resourceCost,
        getCostsList: getCostsList,
        acceptsResource: acceptsResource,
        canPower: canPower,
        incomeForPower: incomeForPower,
        maxPlants: maxPlants,
        getAvailableRegions: getAvailableRegions,
        isCityOpened: isCityOpened,
        comparePlayerPlants: comparePlayerPlants,
        shouldPlayNotification: shouldPlayNotification
    };
});