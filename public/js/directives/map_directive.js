'use strict';

angular.module('powergrid')
.directive('map', function() {
    return {
        restrict: 'E',
        templateUrl: '/js/directives/map.html'
    };
});