'use strict';

angular.module('powergrid')
.directive('auction', function() {
    return {
        restrict: 'E',
        templateUrl: '/js/directives/auction.html'
    };
});