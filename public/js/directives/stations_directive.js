'use strict';

angular.module('powergrid')
.directive('stations', function() {
    return {
        restrict: 'E',
        templateUrl: '/js/directives/stations.html'
    };
});