var expressSession = require('express-session');

module.exports = expressSession({secret: 'dunnoWhatSecretKeyShouldBeUsed', resave: true, saveUninitialized: true});