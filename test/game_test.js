var expect = require('chai').expect;

var dbConfig = require('../db/config');
var mongoose = require('mongoose');
mongoose.connect(dbConfig.url);

var consts = require('../models/consts');
var User = require('../models/user');
var Lobby = require('../models/lobby');
var Game = require('../models/game');
var Plant = require('../models/plant')
var Map = require('../models/map');

var player1;
var player2;
var player3;
var lobby;
var usaMap;
var game;

function sampleAuction() {
    game.round = 2;
	var plants = game.getAvailablePlants();
	game.pickPlantForAuction(player1, plants[0]);  // Plant 3 - 1 for 2 oil
	game.passForPlant(player2);
	game.passForPlant(player3);
	game.pickPlantForAuction(player2, plants[1]);  // Plant 4 - 1 for 2 coal
	game.passForPlant(player3);
	game.pickPlantForAuction(player3, plants[2]);  // Plant 5 - 1 for 2 hybrid

	game.startAuctionState();
	plants = game.getAvailablePlants();
	game.pickPlantForAuction(player1, plants[1]);  // Plant 7 - 2 for 3 oil
	game.passForPlant(player2);
	game.passForPlant(player3);
	game.pickPlantForAuction(player2, plants[2]);  // Plant 8 - 2 for 3 coal
	game.passForPlant(player3);
	game.pickPlantForAuction(player3, plants[3]);  // Plant 9 - 1 for 1 oil
}

function sampleResources() {
	game.endResourceBuy(player1);
	game.endResourceBuy(player2);
	game.endResourceBuy(player3);
}

function sampleCities() {
	game.buyCitiesForUser(player3, []);
	game.buyCitiesForUser(player2, []);
	game.buyCitiesForUser(player1, []);
}

describe('Game tests#', function() {

    before(function(done) {
        player1 = new User();
        player1.username = 'player1';
        player2 = new User();
        player2.username = 'player2';
        player3= new User();
        player3.username = 'player3';

        lobby = new Lobby();
        lobby.name = 'Test lobby';
        lobby.maxPlayers = 3;
        lobby.owner = player1._id;
        lobby.players.push(player1._id);
        lobby.players.push(player2._id);
        lobby.players.push(player3._id);

        Map.findOne({ name: 'USA' })
        .exec(function(err, map) {
            usaMap = map;
            lobby.setMap(map);

            game = new Game();
            game.initGame(lobby);

            game.chooseRegion(player1._id, 1);
            game.chooseRegion(player2._id, 2);
            game.chooseRegion(player3._id, 3);

            player1.save(function(err) {
                player2.save(function(err) {
                    player3.save(function(err) {
                        game.save(function(err) {
                            done();
                        });
                    });
                });
            });
        });
    });

    after(function(done) {
        player1.remove(function(err) {
            player2.remove(function(err) {
                player3.remove(function(err) {
                    game.remove(function(err) {
                        done();
                    });
                });
            });
        })
    });

    beforeEach(function(done) {
        Game.findOne({ '_id': game.id })
        .exec(function(err, found) {
            game = found;
            done();
        });
    });

    describe('Plant model tests#', function() {
    	it('should accept oil', function() {
			var plants = game.getAvailablePlants();
			expect(plants[0].acceptsResource('oil')).to.equal(true);
			expect(plants[0].acceptsResource('coal')).to.equal(false);
			expect(plants[0].acceptsResource('garbage')).to.equal(false);
			expect(plants[0].acceptsResource('uranium')).to.equal(false);
		});

		it('should accept coal and oil', function() {
			var plants = game.getAvailablePlants();
			expect(plants[2].acceptsResource('oil')).to.equal(true);
			expect(plants[2].acceptsResource('coal')).to.equal(true);
			expect(plants[2].acceptsResource('garbage')).to.equal(false);
			expect(plants[2].acceptsResource('uranium')).to.equal(false);
		});
    });

    describe('Plant tests#', function() {
        it('should return available plants', function() {
            var plants = game.getAvailablePlants();
            expect(plants).to.have.length(4);
            expect(plants[0].cost).to.equal(3);
            expect(plants[1].cost).to.equal(4);
            expect(plants[2].cost).to.equal(5);
            expect(plants[3].cost).to.equal(6);
        });

        it('should remove plant from market and replace it', function() {
            var plants = game.getAvailablePlants();
            var plantToRemove = plants[0];
            game.removePlantFromMarket(plantToRemove);
            plants = game.getAvailablePlants();
            expect(plants).to.have.length(4);
            expect(plants[0].cost).not.to.equal(plantToRemove.cost);
            expect(plants[0].cost).to.be.below(plants[1].cost);
            expect(plants[1].cost).to.be.below(plants[2].cost);
            expect(plants[2].cost).to.be.below(plants[3].cost);
        });

        it('should allow to buy plant', function() {
            var plants = game.getAvailablePlants();
            game.buyPlant(player1, plants[0], plants[0].cost);
            var player = game.getPlayer(player1._id);
            expect(player.money).to.equal(50 - plants[0].cost);
            expect(player.plants).to.have.length(1);
            expect(player.plants[0].plant.cost).to.equal(plants[0].cost);
        });

        it('should allow to discard plant', function() {
            var plants = game.getAvailablePlants();
            game.buyPlant(player1, plants[0], plants[0].cost);
            var player = game.getPlayer(player1._id);
            game.discardPlant(player1, player.plants[0]);
            expect(player.plants).to.have.length(0);
        });
    });

    describe('Auction tests#', function() {
    	beforeEach(function() {
    		game.round = 2;
    	});

        it('should return players in order', function() {
            game.players[0].order = 1;
            game.players[1].order = 0;
            game.players[2].order = 2;
            var order = game.getOrder();
            expect(order[0]).that.deep.equals(game.players[1].user);
            expect(order[1]).that.deep.equals(game.players[0].user);
            expect(order[2]).that.deep.equals(game.players[2].user);
        })

        it('should pick plant for auction', function() {
            var plants = game.getAvailablePlants();
            game.pickPlantForAuction(player1, plants[0]);
            expect(game.auction.target.cost).to.equal(plants[0].cost);
            expect(game.auction.stake).to.equal(plants[0].cost);
            expect(game.auction.biddingPlayers).to.have.length(3);
        });

        it('should move auction', function() {
            var plants = game.getAvailablePlants();
            game.pickPlantForAuction(player1, plants[0]);
            var bidding = game.auction.biddingPlayers.slice(0);
            game.moveAuction();
            expect(game.auction.biddingPlayers).to.have.length(3);
            expect(game.auction.biddingPlayers[0]).that.deep.equals(bidding[1]);
            expect(game.auction.biddingPlayers[1]).that.deep.equals(bidding[2]);
            expect(game.auction.biddingPlayers[2]).that.deep.equals(bidding[0]);
        });        

        it('should end auction', function() {
            var plants = game.getAvailablePlants();
            game.pickPlantForAuction(player1, plants[0]);
            game.auction.biddingPlayers = [player1._id];
            game.moveAuction();
            var player = game.getPlayer(player1._id);
            expect(player.plants).to.have.length(1);
            expect(player.plants[0].plant.cost).to.equal(plants[0].cost);
            expect(game.auction.validPlayers).to.have.length(2);
            expect(game.auction.validPlayers[0]).that.deep.equals(player2._id);
            expect(game.auction.validPlayers[1]).that.deep.equals(player3._id);
            expect(game.auction.target).to.equal(null);
        });

        it('should bid for plant', function() {
        	var plants = game.getAvailablePlants();
            game.pickPlantForAuction(player1, plants[0]);
            game.bidForPlant(player2, 10);
            expect(game.auction.stake).to.equal(10);
            expect(game.auction.biddingPlayers).to.have.length(3);
            expect(game.auction.biddingPlayers[0]).that.deep.equals(player3._id);
            expect(game.auction.biddingPlayers[1]).that.deep.equals(player1._id);
            expect(game.auction.biddingPlayers[2]).that.deep.equals(player2._id);
            game.bidForPlant(player3, 11);
            expect(game.auction.stake).to.equal(11);
            expect(game.auction.biddingPlayers).to.have.length(3);
            expect(game.auction.biddingPlayers[0]).that.deep.equals(player1._id);
            expect(game.auction.biddingPlayers[1]).that.deep.equals(player2._id);
            expect(game.auction.biddingPlayers[2]).that.deep.equals(player3._id);
        });

        it('should pass for plant during auction', function() {
        	var plants = game.getAvailablePlants();
            game.pickPlantForAuction(player1, plants[0]);
            game.passForPlant(player2);
            expect(game.auction.stake).to.equal(plants[0].cost);
            expect(game.auction.biddingPlayers).to.have.length(2);
            expect(game.auction.biddingPlayers[0]).that.deep.equals(player3._id);
            expect(game.auction.biddingPlayers[1]).that.deep.equals(player1._id);
            game.passForPlant(player3);
            var player = game.getPlayer(player1._id);
            expect(player.plants).to.have.length(1);
            expect(player.plants[0].plant.cost).to.equal(plants[0].cost);
            expect(game.auction.validPlayers).to.have.length(2);
            expect(game.auction.validPlayers[0]).that.deep.equals(player2._id);
            expect(game.auction.validPlayers[1]).that.deep.equals(player3._id);
            expect(game.auction.target).to.equal(null);
        });

		it('should pass for plant during picking', function() {
			game.passForPlant(player1);
			expect(game.auction.target).to.equal(null);
			expect(game.auction.validPlayers).to.have.length(2);
			expect(game.auction.validPlayers[0]).that.deep.equals(player2._id);
			expect(game.auction.validPlayers[1]).that.deep.equals(player3._id);
			game.passForPlant(player2);
			expect(game.auction.target).to.equal(null);
			expect(game.auction.validPlayers).to.have.length(1);
			expect(game.auction.validPlayers[0]).that.deep.equals(player3._id);
			game.passForPlant(player3);
			expect(game.auction.target).to.equal(null);
			expect(game.auction.validPlayers).to.have.length(0);
			expect(game.state).to.equal('resources');
		});

		it('should force to buy in first round', function() {
			game.round = 1;
			game.passForPlant(player1);
			expect(game.auction.validPlayers).to.have.length(3);
			var plants = game.getAvailablePlants();
            game.pickPlantForAuction(player1, plants[0]);
            expect(game.auction.target.cost).to.equal(plants[0].cost);
            expect(game.auction.stake).to.equal(plants[0].cost);
            expect(game.auction.biddingPlayers).to.have.length(3);
		});

        it('should advance to third phase after auction', function() {
            var plants = game.getAvailablePlants();
            game.stack.splice(8, game.stack.length - 8);
            game.passForPlant(player1);
            game.pickPlantForAuction(player2, plants[0]);
            game.passForPlant(player3);
            expect(game.phase).to.equal(1);
            game.pickPlantForAuction(player3, plants[1]);
            expect(game.phase).to.equal(3);
        });

        it('should not change state when player has to many plants', function() {
            var plants = game.getAvailablePlants();
            var player = game.getPlayer(player1._id);
            game.pickPlantForAuction(player1, plants[0]);
            game.passForPlant(player2);
            game.passForPlant(player3);
            player.plants.push(player.plants[0]);
            player.plants.push(player.plants[0]);
            player.plants.push(player.plants[0]);
            game.passForPlant(player2);
            game.passForPlant(player3);
            expect(game.state).to.equal('auction');
            game.discardPlant(player1, player.plants[0]);
            expect(game.state).to.equal('resources');
        });

        it('should sort plants properly after move', function() {
            var plants = game.getAvailablePlants();
            game.stack[8] = game.map.plants[30];
            game.stack[9] = game.map.plants[29];
            game.stack[10] = game.map.plants[28];
            game.stack[11] = game.map.plants[27];
            var checkMarket = function() {
                for (var i = 0; i < 7; ++i) {
                    expect(game.stack[i + 1].cost).to.be.above(game.stack[i].cost);
                }
            }

            checkMarket();
            game.pickPlantForAuction(player1, plants[0]);
            game.passForPlant(player2);
            game.passForPlant(player3);
            checkMarket();
            game.pickPlantForAuction(player2, plants[1]);
            game.passForPlant(player3);
            checkMarket();
            game.pickPlantForAuction(player3, plants[2]);
            checkMarket();
        });
    });

	describe('Resource tests#', function() {

		beforeEach(function() {
			sampleAuction();
		});

		it('should move oil between plants', function() {
			var player = game.getPlayer(player1._id);
			player.plants[0].oil = 2;
			game.moveResourceBetweenPlants('oil', 1, player.plants[0], player.plants[1]);
			expect(game.players[0].plants[0].oil).to.equal(1);
			expect(game.players[0].plants[1].oil).to.equal(1);
		});

		it('should move oil between hybrid plant', function() {
			var player = game.getPlayer(player3._id);
			player.plants[0].oil = 2;
			game.moveResourceBetweenPlants('oil', 1, player.plants[0], player.plants[1]);
			expect(game.players[2].plants[0].oil).to.equal(1);
			expect(game.players[2].plants[1].oil).to.equal(1);
		});

		it('should compute coal cost', function() {
			expect(game.resourceCost(consts.COAL, 1)).to.equal(1);
			expect(game.resourceCost(consts.COAL, 2)).to.equal(2);
			expect(game.resourceCost(consts.COAL, 3)).to.equal(3);
			expect(game.resourceCost(consts.COAL, 4)).to.equal(5);
			expect(game.resourceCost(consts.COAL, 5)).to.equal(7);
			expect(game.resourceCost(consts.COAL, 6)).to.equal(9);
			expect(game.resourceCost(consts.COAL, 7)).to.equal(12);
			expect(game.resourceCost(consts.COAL, 100)).to.equal(-1);
		});

		it('should compute uranium cost', function() {
			expect(game.resourceCost(consts.URANIUM, 1)).to.equal(14);
			expect(game.resourceCost(consts.URANIUM, 2)).to.equal(30);
			expect(game.resourceCost(consts.URANIUM, 3)).to.equal(-1);
		});

		it('should buy oil', function() {
			var player = game.getPlayer(player3._id);
			game.buyResources('oil', player3, player.plants[0], 2);
			expect(game.oil).to.equal(16);
			expect(player.money).to.equal(30);
			expect(player.plants[0].oil).to.equal(2);
		});

		it('should not allow to buy', function() {
			var player = game.getPlayer(player2._id);
			var money = player.money
			game.buyResources('coal', player2, player.plants[0], 2);
			expect(game.coal).to.equal(24);
			expect(player.money).to.equal(money);
			expect(player.plants[0].coal).to.equal(0);
		});

		it('should end buying', function() {
			game.endResourceBuy(player1);
			expect(game.buyingResources).to.have.length(2);
		});

		it('should end resources state', function() {
			game.endResourceBuy(player1);
			game.endResourceBuy(player2);
			game.endResourceBuy(player3);
			expect(game.buyingResources).to.have.length(0);
			expect(game.state).to.equal('stations');
		});
	});

	describe('Cities tests#', function() {

		beforeEach(function() {
			sampleAuction();
			sampleResources();
		});

		it('should return city', function() {
			for (city of game.map.cities) {
				var found = game.getCity(city.id);
				expect(found).to.equal(city);
			}
		});

		it('should return connection cost', function() {
			for (city of game.map.cities) {
				for (connection of city.connections) {
					var cost = game.getConnectionCost(city.id, connection.target);
					expect(connection.cost).to.equal(cost);
				}
			}
		});

		it('shoudl return cost of city', function() {
			expect(game.getCityBuyingCosts()[0]).to.equal(10);
			game.players[0].cities.push(0);
			expect(game.getCityBuyingCosts()[0]).to.equal(-1);
			game.phase = 2;
			expect(game.getCityBuyingCosts()[0]).to.equal(15);
		});

		it('should return cities cost', function() {
			var seatle = game.getCityByName('Seatle');
			var portland = game.getCityByName('Portland');
			var boise = game.getCityByName('Boise');
			var billings = game.getCityByName('Billings');
			var cheyenne = game.getCityByName('Cheyenne');
			
			var costs = game.getCostsList(game.players[0].cities);
			for (var i = 0; i < costs.length; ++i) {
                if (game.isCityOpened(i)) {
                    expect(costs[i]).to.equal(10);
                } else {
                    expect(costs[i]).to.equal(-1);
                }
			}

			game.players[0].cities.push(seatle.id);
			costs = game.getCostsList(game.players[1].cities);
			for (var i = 0; i < costs.length; ++i) {
				if (game.map.cities[i].id == seatle.id) {
					expect(costs[i]).to.equal(-1);
				} else {
					if (game.isCityOpened(i)) {
                        expect(costs[i]).to.equal(10);
                    } else {
                        expect(costs[i]).to.equal(-1);
                    }
				}
			}

			costs = game.getCostsList(game.players[0].cities);
			expect(costs[seatle.id]).to.equal(-1);
			expect(costs[portland.id]).to.equal(13);
			expect(costs[boise.id]).to.equal(22);
			expect(costs[billings.id]).to.equal(19);
			expect(costs[cheyenne.id]).to.equal(28);

			game.players[0].cities.push(billings.id);
			costs = game.getCostsList(game.players[0].cities);
			expect(costs[seatle.id]).to.equal(-1);
			expect(costs[portland.id]).to.equal(13);
			expect(costs[boise.id]).to.equal(22);
			expect(costs[billings.id]).to.equal(-1);
			expect(costs[cheyenne.id]).to.equal(19);
		});

		it('should buy city', function() {
			var seatle = game.getCityByName('Seatle');
			var cheyenne = game.getCityByName('Cheyenne');
			var player = game.getPlayer(player3._id);
			var startMoney = player.money = 50;
            var plants = game.stack.length;
			
			game.buyCity(player3, seatle.id);
			expect(player.cities).to.have.length(1);
			expect(player.cities.indexOf(seatle.id)).not.to.equal(-1);
			expect(player.money).to.equal(startMoney - 10);
			
			game.buyCity(player3, cheyenne.id);
			expect(player.cities).to.have.length(2);
			expect(player.cities.indexOf(cheyenne.id)).not.to.equal(-1);
			expect(player.money).to.equal(startMoney - 10 - 28);
            expect(game.stack.length).to.equal(plants);
		});

		it('should buy cities for user', function() {
			var seatle = game.getCityByName('Seatle');
			var cheyenne = game.getCityByName('Cheyenne');
			var player = game.getPlayer(player3._id);
			var startMoney = player.money = 50;
            var plants = game.stack.length;

			game.buyCitiesForUser(player3, [seatle.id, cheyenne.id]);
			expect(player.cities).to.have.length(2);
			expect(player.cities.indexOf(seatle.id)).not.to.equal(-1);
			expect(player.cities.indexOf(cheyenne.id)).not.to.equal(-1);
			expect(player.money).to.equal(startMoney - 10 - 28);
            expect(game.stack.length).to.equal(plants);
		});

		it('should not allow to buy cities', function() {
			var seatle = game.getCityByName('Seatle');
			var player = game.getPlayer(player2._id);
			var startMoney = player.money = 50;

			game.buyCitiesForUser(player2, [seatle.id]);
			expect(player.cities).to.have.length(0);
			expect(player.money).to.equal(startMoney);
		});

		it('should end cities state', function() {
			game.buyCitiesForUser(player3, []);
			game.buyCitiesForUser(player2, []);
			game.buyCitiesForUser(player1, []);
			expect(game.state).to.equal('bureaucracy');
			expect(game.poweringCities).to.have.length(3);
            expect(game.phase).to.equal(1);
		});

        it('should end cities state and advance to second phase', function() {
            game.removeCheapPlants(7);
            var player = game.getPlayer(player1._id);
            var cheapestPlant = game.stack[0];
            var length = game.stack.length;
            player.cities = [0, 0, 0, 0, 0, 0, 0];
            game.buyCitiesForUser(player3, []);
            game.buyCitiesForUser(player2, []);
            game.buyCitiesForUser(player1, []);
            expect(game.state).to.equal('bureaucracy');
            expect(game.poweringCities).to.have.length(3);
            expect(game.phase).to.equal(2);
            expect(game.stack.length + 1).to.equal(length);
            expect(game.stack[0].cost).to.not.equal(cheapestPlant.cost);
        });

        it('should end cities state and not advance to second phase', function() {
            game.phase = 3;
            var player = game.getPlayer(player1._id);
            player.cities = [0, 0, 0, 0, 0, 0, 0];
            game.buyCitiesForUser(player3, []);
            game.buyCitiesForUser(player2, []);
            game.buyCitiesForUser(player1, []);
            expect(game.state).to.equal('bureaucracy');
            expect(game.poweringCities).to.have.length(3);
            expect(game.phase).to.equal(3);
        });

        it('should remove cheap plants', function() {
            game.removeCheapPlants(11);
            for (var plant of game.stack) {
                expect(plant.cost).to.be.above(11);
            }
            for (var plant of game.bottomStack) {
                expect(plant.cost).to.be.above(11);
            }
        });

        it('should remove plants cheaper than max cities', function() {
            game.players[0].cities = [0, 0, 0, 0, 0, 0, 0];
            game.players[1].cities = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
            game.players[2].cities = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
            game.checkAndRemoveCheapPlants();
            for (var plant of game.stack) {
                expect(plant.cost).to.be.above(13);
            }
            for (var plant of game.bottomStack) {
                expect(plant.cost).to.be.above(13);
            }
        });
	});

	describe('Bureaucracy tests#', function() {
		
		beforeEach(function() {
			sampleAuction();
			sampleResources();
			sampleCities();
		});

		it('should power cities', function() {
			var player = game.getPlayer(player1._id);
            player.cities.push(0);
            player.cities.push(1);
            var startMoney = player.money;
			player.plants[0].oil = 3;
			var plan = [{'plant': 3, 'coal': 0, 'oil': 2, 'garbage': 0, 'uranium': 0}];
            game.powerCities(player1, plan);
            expect(player.plants[0].oil).to.equal(1);
            expect(player.money).to.equal(startMoney + 22);
            expect(game.poweringCities).to.have.length(2);
		});

        it('should power more than have cities', function() {
            var player = game.getPlayer(player1._id);
            var startMoney = player.money;
            player.plants[0].oil = 3;
            var plan = [{'plant': 3, 'coal': 0, 'oil': 2, 'garbage': 0, 'uranium': 0}];
            game.powerCities(player1, plan);
            expect(player.plants[0].oil).to.equal(1);
            expect(player.money).to.equal(startMoney + 10);
            expect(game.poweringCities).to.have.length(2);
        });

        it('should get money for not powering', function() {
            var player = game.getPlayer(player1._id);
            var startMoney = player.money;
            var plan = [];
            game.powerCities(player1, plan);
            expect(player.money).to.equal(startMoney + 10);
            expect(game.poweringCities).to.have.length(2);
        });

        it('should establish order based on cities', function() {
        	var first = game.getPlayer(player1._id);
        	var second = game.getPlayer(player2._id);
        	var third = game.getPlayer(player3._id);
        	first.cities = [0, 1];
        	second.cities = [2];
        	third.cities = [3, 4, 5];
        	game.establishOrder();
            var order = game.getOrder();
        	expect(order[0]).that.deep.equals(third.user);
        	expect(order[1]).that.deep.equals(first.user);
        	expect(order[2]).that.deep.equals(second.user);
        });

        it('should establish order based on plants', function() {
        	var first = game.getPlayer(player1._id);
        	var second = game.getPlayer(player2._id);
        	var third = game.getPlayer(player3._id);
        	first.cities = [0];
        	second.cities = [1];
        	third.cities = [2];
        	game.establishOrder();
            var order = game.getOrder();
        	expect(order[0]).that.deep.equals(player3._id);
        	expect(order[1]).that.deep.equals(player2._id);
        	expect(order[2]).that.deep.equals(player1._id);
        });

        it('should end bureaucracy state', function() {
            var mostExpensivePlant = game.stack[7];
        	game.powerCities(player2, []);
        	game.powerCities(player1, []);
        	game.powerCities(player3, []);
        	expect(game.state).to.equal(consts.AUCTION);
        	expect(game.round).to.equal(3);
            expect(game.bottomStack).to.have.length(1);
            expect(game.bottomStack[0].cost).to.equal(mostExpensivePlant.cost);
            expect(game.stack[7].cost).to.not.equal(mostExpensivePlant.cost);
        });

        it('should end bureaucracy state in phase 3', function() {
            var cheapestPlant = game.stack[0];
            var stackLength = game.stack.length;
            game.phase = 3;
            game.powerCities(player2, []);
            game.powerCities(player1, []);
            game.powerCities(player3, []);
            expect(game.state).to.equal(consts.AUCTION);
            expect(game.round).to.equal(3);
            expect(stackLength - 1).to.equal(game.stack.length);
            expect(game.stack[0].cost).to.not.equal(cheapestPlant.cost);
        });

        it('should replenish resources in first phase', function() {
            game.coal = 0;
            game.oil = 0;
            game.garbage = 0;
            game.uranium = 0;
            game.phase = 1;
            game.powerCities(player2, []);
            game.powerCities(player1, []);
            game.powerCities(player3, []);
            expect(game.state).to.equal(consts.AUCTION);
            expect(game.coal).to.equal(4);
            expect(game.oil).to.equal(2);
            expect(game.garbage).to.equal(1);
            expect(game.uranium).to.equal(1);
        });

        it('should replenish resources in third phase', function() {
            game.coal = 0;
            game.oil = 0;
            game.garbage = 0;
            game.uranium = 0;
            game.phase = 3;
            game.powerCities(player2, []);
            game.powerCities(player1, []);
            game.powerCities(player3, []);
            expect(game.state).to.equal(consts.AUCTION);
            expect(game.coal).to.equal(3);
            expect(game.oil).to.equal(4);
            expect(game.garbage).to.equal(3);
            expect(game.uranium).to.equal(1);
        });

        it('should not exceed max value after replenishing resources', function() {
            game.coal = 24;
            game.oil = 24;
            game.garbage = 24;
            game.uranium = 12;
            game.phase = 3;
            game.powerCities(player2, []);
            game.powerCities(player1, []);
            game.powerCities(player3, []);
            expect(game.state).to.equal(consts.AUCTION);
            expect(game.coal).to.equal(24);
            expect(game.oil).to.equal(24);
            expect(game.garbage).to.equal(24);
            expect(game.uranium).to.equal(12);
        });

        it('should end round and advance to third phase', function() {
            var cheapestPlant = game.stack[0];
            game.bottomStack = game.stack.slice(0);
            game.stack.splice(7, game.stack.length - 8);
            game.powerCities(player2, []);
            game.powerCities(player1, []);
            game.powerCities(player3, []);
            expect(game.state).to.equal(consts.AUCTION);
            expect(game.phase).to.equal(3);
            expect(game.stack.length).to.not.equal(7);
            expect(game.stack[0].cost).to.not.equal(cheapestPlant.cost);
        });

        it('should end game', function() {
            var player = game.getPlayer(player1._id);
            player.cities = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16];
            game.powerCities(player2, []);
            game.powerCities(player1, []);
            game.powerCities(player3, []);
            expect(game.state).to.equal('end');
            expect(game.lastPowering[0].poweredCities).to.equal(0);
            expect(game.lastPowering[1].poweredCities).to.equal(0);
            expect(game.lastPowering[2].poweredCities).to.equal(0);
        });

        it('should remember last powering', function() {
            var player = game.getPlayer(player1._id);
            player.cities.push(0);
            player.cities.push(1);
            var startMoney = player.money;
            player.plants[0].oil = 3;
            var plan = [{'plant': 3, 'coal': 0, 'oil': 2, 'garbage': 0, 'uranium': 0}];
            game.powerCities(player2, []);
            game.powerCities(player1, plan);
            game.powerCities(player3, []);
            expect(game.lastPowering[0].poweredCities).to.equal(0);
            expect(game.lastPowering[1].poweredCities).to.equal(1);
            expect(game.lastPowering[1].user).that.deep.equals(player1._id);
            expect(game.lastPowering[2].poweredCities).to.equal(0);
        });

        it('should tell who won based on powered cities', function() {
            game.state = 'end';
            game.lastPowering = [{
                user: player1._id, 
                poweredCities: 10
            }, {user: player2._id,
                poweredCities: 15
            }, {user: player3._id, 
                poweredCities: 14}];
            var winner = game.whoWon();
            expect(winner).that.deep.equals(player2._id);
        });

        it('should tell who won based on money', function() {
            game.state = 'end';
            game.lastPowering = [{
                user: player1._id, 
                poweredCities: 15
            }, {user: player2._id,
                poweredCities: 15
            }, {user: player3._id, 
                poweredCities: 15}];
            var player;
            player = game.getPlayer(player1._id);
            player.money = 50;
            player = game.getPlayer(player2._id);
            player.money = 40;
            player = game.getPlayer(player3._id);
            player.money = 55;
            var winner = game.whoWon();
            expect(winner).that.deep.equals(player3._id);
        });

        it('should tell who won based on cities', function() {
            game.state = 'end';
            game.lastPowering = [{
                user: player1._id, 
                poweredCities: 15
            }, {user: player2._id,
                poweredCities: 15
            }, {user: player3._id, 
                poweredCities: 15}];
            var player;
            player = game.getPlayer(player1._id);
            player.money = 50;
            player.cities = [0, 1, 2, 3, 4, 5, 6, 7];
            player = game.getPlayer(player2._id);
            player.money = 50;
            player.cities = [0, 1, 2, 3, 4, 5];
            player = game.getPlayer(player3._id);
            player.money = 50;
            player.cities = [0, 1, 2, 3, 4, 6];
            var winner = game.whoWon();
            expect(winner).that.deep.equals(player1._id);
        });
	});

    describe('Sample round#', function() {
        it('should create lobby', function() {
            expect(lobby.name).to.equal('Test lobby');
            expect(lobby.maxPlayers).to.equal(3);
            expect(lobby.owner).to.equal(player1._id);
            expect(lobby.players).to.have.length(3);
            expect(lobby.players).to.contain(player1._id);
            expect(lobby.players).to.contain(player2._id);
            expect(lobby.players).to.contain(player3._id);
            expect(lobby.map.name).to.equal(usaMap.name);
        });

        it('should create game', function() {
            expect(game.name).to.be.equal('Test lobby');
            expect(game.players).to.have.length(3);

            expect(game.getPlayer(player1._id).user).that.deep.equals(player1._id);
            expect(game.getPlayer(player1._id).money).to.equal(50);
            expect(game.getPlayer(player1._id).plants).to.have.length(0);
            expect(game.getPlayer(player1._id).cities).to.have.length(0);

            expect(game.getPlayer(player2._id).user).that.deep.equals(player2._id);
            expect(game.getPlayer(player2._id).money).to.equal(50);
            expect(game.getPlayer(player2._id).plants).to.have.length(0);
            expect(game.getPlayer(player2._id).cities).to.have.length(0);

            expect(game.getPlayer(player3._id).user).that.deep.equals(player3._id);
            expect(game.getPlayer(player3._id).money).to.equal(50);
            expect(game.getPlayer(player3._id).plants).to.have.length(0);
            expect(game.getPlayer(player2._id).cities).to.have.length(0);

            var order = game.getOrder();
            expect(order[0]).that.deep.equals(player1._id);
            expect(order[1]).that.deep.equals(player2._id);
            expect(order[2]).that.deep.equals(player3._id);

            expect(game.state).to.equal('auction');
            expect(game.coal).to.equal(24);
            expect(game.oil).to.equal(18);
            expect(game.garbage).to.equal(6);
            expect(game.uranium).to.equal(2);

            expect(game.stack[0].cost).to.equal(3); 
            expect(game.stack[1].cost).to.equal(4); 
            expect(game.stack[2].cost).to.equal(5); 
            expect(game.stack[3].cost).to.equal(6); 
            expect(game.stack[4].cost).to.equal(7); 
            expect(game.stack[5].cost).to.equal(8); 
            expect(game.stack[6].cost).to.equal(9); 
            expect(game.stack[7].cost).to.equal(10); 
            expect(game.stack[8].cost).to.equal(13); 
            expect(game.stack).to.have.length(34);
            expect(game.bottomStack).to.have.length(0);
            expect(game.round).to.equal(1);

            // console.log(game);
        });

        it('should pass auction', function() {
        	game.round = 2;
            var plants = game.getAvailablePlants();
            game.pickPlantForAuction(player1, plants[1]);
            expect(game.auction.target.cost).to.equal(4);
            expect(game.auction.stake).to.equal(4);
            game.bidForPlant(player2, 6);
            expect(game.auction.stake).to.equal(6);
            game.bidForPlant(player3, 7);
            expect(game.auction.stake).to.equal(7);
            game.bidForPlant(player1, 8);
            expect(game.auction.stake).to.equal(8);
            game.bidForPlant(player2, 10);
            expect(game.auction.stake).to.equal(10);
            game.passForPlant(player3);
            game.passForPlant(player1);
            expect(game.auction.target).to.equal(null);
            expect(game.players[1].plants).to.have.length(1);
            expect(game.players[1].plants[0].plant.cost).to.equal(4);
            expect(game.players[1].money).to.equal(40);

            game.passForPlant(player1);
            expect(game.auction.target).to.equal(null);
            expect(game.players[0].plants).to.have.length(0);
            expect(game.players[0].money).to.equal(50);

            game.pickPlantForAuction(player3, plants[2]);
            expect(game.auction.target).to.equal(null);
            expect(game.players[2].plants).to.have.length(1);
            expect(game.players[2].plants[0].plant.cost).to.equal(5);
            expect(game.players[2].money).to.equal(45);
            expect(game.state).to.equal('resources');
        });
    });
});