var expect = require('chai').expect;

describe('Addition', function() {
    it('2+2=4', function() {
        var two = 2;
        var four = 4;

        expect(two + two).to.equal(four);
        expect(two + two).to.deep.equal(four);
    });
});